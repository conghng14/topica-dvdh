<?php
return [
	'list_services' => 
	[
		'mau-phan-hoi-ve-don-hang-hop-dong' => [
			'title'		=> 	'Phản hồi về đơn hàng hơp đồng',
			'content' 	=>	'Contact ID:<br />
							Họ và tên học viên:<br />
							Số điện thoại học viên:<br />
							Thông tin phản ánh về hợp đồng:'
		],
		'hoa-don-do-moi' => [
			'title' 	=> 	'Hóa đơn đỏ mới',
			'content' 	=> 	'<b>THÔNG TIN HỌC VIÊN</b><br />	
								Contact ID:<br />
								Loại học viên:<br />
								Họ và tên học viên:<br />
								Số điện thoại học viên:<br />
								Vùng miền:<br />
								Tên gói học:<br />
								Hoàn thiện học phí:<br />
								Loại hình thu học phí:<br />
								Số tiền:<br />
								Tặng phẩm:<br /><br />
							<b>THÔNG TIN GỬI HỢP ĐỒNG</b><br />	
								Tên người nộp học phí:<br />
								Số điện thoại người nộp học phí:<br />
								Địa chỉ thu học phí:<br />
								Thời gian thu học phí:<br />
								Ghi chú:'
		],
		'phan-hoi-ve-don-hang-hoa-don-do' => [
			'title' 	=> 	'Phản hồi về đơn hàng hóa đơn đỏ',
			'content' 	=> 	'Nhập nội dung tại đây...'
		],
		'thu-tien-tai-nha' => [
			'title' 	=> 	'Thu tiền tại nhà',
			'content' 	=> 	'<b>THÔNG TIN HỌC VIÊN</b><br />
								Contact ID:<br />
								Loại học viên:<br />
								Họ và tên học viên:<br />
								Số điện thoại học viên:<br />
								Vùng miền:<br />
								Tên gói học:<br />
								Hoàn thiện học phí:<br />
								Hình thức thanh toán:<br />
								Số tiền:<br />
								Tặng phẩm:<br />
								Dịch vụ đi kèm:<br /><br />
							<b>THÔNG TIN GỬI HỢP ĐỒNG</b><br />
								Tên người nộp học phí:<br />
								Số điện thoại người nộp học phí:<br />
								Địa chỉ thu học phí:<br />
								Thời gian thu học phí:<br />
								Ghi chú:'
		],
		'tang-qua-sau-thanh-toan' => [
			'title' 	=> 	'Tặng quà sau thanh toán',
			'content' 	=> 	'<b>THÔNG TIN HỌC VIÊN</b><br />
								Contact ID:<br />
								Loại học viên:<br />
								Họ và tên học viên:<br />
								Số điện thoại học viên:<br />
								Vùng miền:<br />
								Khóa học:<br />
								Thông tin thanh toán học phí:<br />
								Tặng phẩm:<br />
								Dịch vụ đi kèm:<br /><br />
							<b>THÔNG TIN GỬI HỢP ĐỒNG</b><br />
								Tên người nhận hàng:<br />
								Số điện thoại người nhận hàng:<br />
								Địa chỉ nhận hàng:<br />
								Thời gian nhận hàng:<br />
								Ghi chú:'
		],
		'tang-qua-truoc-thanh-toan' => [
			'title' 	=> 	'Tặng quà trước thanh toán',
			'content' 	=> 	'<b>THÔNG TIN HỌC VIÊN</b><br />
								Contact ID:<br />
								Loại học viên:<br />
								Họ và tên học viên:<br />
								Số điện thoại học viên:<br />
								Vùng miền:<br />
								Khóa học:<br />
								Dự kiến ngày nộp học phí:<br />
								Tặng phẩm:<br />
								Dịch vụ đi kèm:<br /><br />
							<b>THÔNG TIN GỬI HỢP ĐỒNG</b><br />
								Tên người nhận hàng:<br />
								Số điện thoại người nhận hàng:<br />
								Địa chỉ nhận hàng:<br />
								Thời gian nhận hàng:<br />
								Ghi chú:'
		],
		'phan-hoi-ve-don-hang-cod' => [
			'title' 	=> 	'Phản hồi về đơn hàng COD',
			'content' 	=> 	'Contact ID:<br />
								Họ và tên học viên:<br />
								Số điện thoại học viên:<br />
								Thông tin phản ánh về đơn hàng COD:'
		],
		'rut-hoc-phi' => [
			'title'		=> 	'Rút học phí',
			'content' 	=> 	'Contact ID:<br />
								Loại học viên:<br />
								Họ và tên học viên:<br />
								Số điện thoại học viên:<br />
								Nội dung thanh toán học phí:<br />
								Tên gói học:<br />
								Lý do hoàn học phí:<br />
								Số tiền cần trả lại:<br />
								Thông tin tài khoản nhận tiền:'
		],
		'thac-mac-ve-com' => [
			'title' 	=> 	'Thắc mắc về COM',
			'content' 	=> 	'Nội dung thắc mắc về COM:'
		],
		'dat-phong-hop' => [
			'title' 	=> 	'Đặt phòng họp',
			'content' 	=> 	'Thời gian họp:<br />
								Số lượng nhân sự tham gia:<br />
								Dụng cụ cần trong buổi họp:<br />
								Nội dung họp:'
		],
		'in-ho' => [
			'title' 	=> 	'In hộ',
			'content' 	=> 	'Nhập nội dung tại đây...'
		],
		'bo-sung-cham-cong' => [
			'title' 	=> 	'Bổ sung chấm công',
			'content' 	=> 	'Nội dung đơn hàng bổ sung chấm công:<br />
								Thời gian chấm công bổ sung:<br />
								Lý do quên chấm công:'
		],
		'don-hang-khac' => [
			'title' 	=> 	'Đơn hàng khác',
			'content' 	=> 	'Nôi dung đơn hàng:'
		],
		'cts-sai-so-khong-cap-nhat-duoc-crm' => [
			'title' 	=> 	'CTS sai số- Không cập nhập được CRM',
			'content' 	=> 	'ID học viên:<br />
								Tên học viên:<br />
								Số điện thoại học viên bị sai:<br />
								Nội dung:'
		],
		'phan-bu-cts' => [
			'title' 	=> 	'Phân bù cts',
			'content' 	=> 	'Số lượng cts lỗi:<br />
								Lý do phân bù:<br />
								Upload danh sách đính kèm:'
		],
		'check-ma-voucher' => [
			'title' 	=> 	'Check mã voucher',
			'content' 	=> 	'ID cts:<br />
								Họ và tên HV:<br />
								Số điện thoại HV:<br />
								Nội dung đơn hàng:'
		],
		'check-trung-cts' => [
			'title' 	=> 	'Check trùng cts',
			'content' 	=> 	'ID cts:<br />
								Họ và tên HV bị trùng:<br />
								Số điện thoại HV bị trùng:<br />
								Nội dung đơn hàng:'
		],
		'khuou-nai-diem-nong' => [
			'title' 	=> 	'Khướu nại - Điểm nóng',
			'content' 	=> 	'Vấn đề khướu nại:<br />
								Nội dung khướu nại:<br />
								Email quản lý trực tiếp:'
		]
	]
];