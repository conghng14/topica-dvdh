var ChuSo=new Array(" không"," một"," hai"," ba"," bốn"," năm"," sáu"," bảy"," tám"," chín");
var Tien=new Array( "", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ");


function numberToString(numB)
{
    var str = "" + numB;
    var strArr= str.split(",");
    if(strArr.length > 1){
        var arrCount = strArr.length;
        var tram = threeNumberToString(strArr[arrCount - 1]);
        var nghin = threeNumberToString(strArr[arrCount - 2]) + " nghìn";
        return jsUcfirst($.trim(nghin + tram));
    }else{
        return jsUcfirst($.trim(threeNumberToString(numB)));
    }    
}

function jsUcfirst(string) 
{
    string = $.trim(string);
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function threeNumberToString(baso)
{
    var nghin;
    var tram;
    var chuc;
    var donvi;
    var KetQua="";
    tram=parseInt(baso/100);
    chuc=parseInt((baso%100)/10);
    donvi=baso%10;
    if(tram==0 && chuc==0 && donvi==0) return "";
    if(tram!=0)
    {
        KetQua += ChuSo[tram] + " trăm";
        if ((chuc == 0) && (donvi != 0)) KetQua += " linh";
    }
    if ((chuc != 0) && (chuc != 1))
    {
            KetQua += ChuSo[chuc] + " mươi";
            if ((chuc == 0) && (donvi != 0)) KetQua = KetQua + " linh";
    }
    if (chuc == 1) KetQua += " mười";
    switch (donvi)
    {
        case 1:
            if ((chuc != 0) && (chuc != 1))
            {
                KetQua += " mốt";
            }
            else
            {
                KetQua += ChuSo[donvi];
            }
            break;
        case 5:
            if (chuc == 0)
            {
                KetQua += ChuSo[donvi];
            }
            else
            {
                KetQua += " lăm";
            }
            break;
        default:
            if (donvi != 0)
            {
                KetQua += ChuSo[donvi];
            }
            break;
        }
    return jsUcfirst(KetQua);
}

function numberToCurrency(SoTien)
{
    var lan=0;
    var i=0;
    var so=0;
    var KetQua="";
    var tmp="";
    var ViTri = new Array();
    if(SoTien<0) return "Số tiền âm !";
    if(SoTien==0) return "Không đồng !";
    if(SoTien>0)
    {
        so=SoTien;
    }
    else
    {
        so = -SoTien;
    }
    if (SoTien > 8999999999999999)
    {
        //SoTien = 0;
        return "Số quá lớn!";
    }
    ViTri[5] = Math.floor(so / 1000000000000000);
    if(isNaN(ViTri[5]))
        ViTri[5] = "0";
    so = so - parseFloat(ViTri[5].toString()) * 1000000000000000;
    ViTri[4] = Math.floor(so / 1000000000000);
     if(isNaN(ViTri[4]))
        ViTri[4] = "0";
    so = so - parseFloat(ViTri[4].toString()) * 1000000000000;
    ViTri[3] = Math.floor(so / 1000000000);
     if(isNaN(ViTri[3]))
        ViTri[3] = "0";
    so = so - parseFloat(ViTri[3].toString()) * 1000000000;
    ViTri[2] = parseInt(so / 1000000);
     if(isNaN(ViTri[2]))
        ViTri[2] = "0";
    ViTri[1] = parseInt((so % 1000000) / 1000);
     if(isNaN(ViTri[1]))
        ViTri[1] = "0";
    ViTri[0] = parseInt(so % 1000);
  if(isNaN(ViTri[0]))
        ViTri[0] = "0";
    if (ViTri[5] > 0)
    {
        lan = 5;
    }
    else if (ViTri[4] > 0)
    {
        lan = 4;
    }
    else if (ViTri[3] > 0)
    {
        lan = 3;
    }
    else if (ViTri[2] > 0)
    {
        lan = 2;
    }
    else if (ViTri[1] > 0)
    {
        lan = 1;
    }
    else
    {
        lan = 0;
    }
    for (i = lan; i >= 0; i--)
    {
       tmp = threeNumberToString(ViTri[i]);
       KetQua += tmp;
       if (ViTri[i] > 0) KetQua += Tien[i];
       //if ((i > 0) && (tmp.length > 0)) KetQua += ',';//&& (!string.IsNullOrEmpty(tmp))
       if ((i > 0) && (tmp.length > 0)) KetQua += ' ';
    }
   // if (KetQua.substring(KetQua.length - 1) == ',')
   // {
   //      KetQua = KetQua.substring(0, KetQua.length - 1);
   // }
   KetQua = KetQua.toLowerCase();
   //KetQua = KetQua.substring(1,2).toUpperCase()+ KetQua.substring(2);
   return jsUcfirst(KetQua);//.substring(0, 1);//.toUpperCase();// + KetQua.substring(1);
}

$(document).on('change', 'select.thoi_gian_dao_tao', function() {
    $('.thoi_gian_dao_tao_bang_chu').val(numberToString(this.value));
});
$(document).on('change', 'select.thoi_gian_bao_luu', function() {
    $('.thoi_gian_bao_luu_bang_chu').val(numberToString(this.value));
});
$(document).on('change', 'select.so_lan_bao_luu', function() {
    $('.so_lan_bao_luu_bang_chu').val(numberToString(this.value));
});
$(document).on('change', 'select.hoc_phi_goc', function() {
    var val = this.value.replace(new RegExp(",", "g"), '');    
    $('.hoc_phi_goc_bang_chu').val(numberToCurrency(val));
});

$(document).on('change', '.hoc_phi_uu_dai', function() {
    var val = this.value.replace(new RegExp(",", "g"), '');
    $('.hoc_phi_uu_dai_bang_chu').val(numberToCurrency(val));
});