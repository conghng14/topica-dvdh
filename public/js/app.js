
function changeStatus(order_id, status) {
    $str_alert = "Order sẽ được gửi tới bộ phận xử lý. Bạn có chắc chắn?";
    if(status == "DONE"){
        $str_alert = "Bạn muốn hoàn thành Order này?";
    }else if(status == "NEW"){
        $str_alert = "Order này sẽ bị hủy và gửi lại cho Tư Vấn Tuyển Sinh. Bạn có chắc chắn?";
    }else if(status == "REJECTED"){
        $str_alert = "Từ chối Order";
    }else if(status == "RECEIVED"){
        $str_alert = "Bạn sẽ là người xử lý Order này. Bạn có chắc chắn?";
    }

    if(confirm($str_alert)){
        $.get("/orders/change-status/"+ order_id +"/"+ status, function(status){
            if(status){
                if(status == "NOT_DONE"){
                    alert("Bạn cần hoàn tất quy trình để hoàn thành Order này.");
                    return false;
                }else if(status == "NO_PERMISSION"){
                    alert("Bạn không không có quyền.");
                    return false;
                }else{
                    $('.' + order_id).text(status);
                    $('.send_'+ order_id).hide();
                    //if(status != 'PENDING'){
                        location.reload();
                    //}
                }                
            }
        });
    }
}

function rejectedOrder(){
    //$('#rejected .modal-body').html(data);
    $('#rejected .modal').modal('toggle');
}