<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index')->name('home');
	// orders
	Route::get('orders-edit-data', 'OrdersController@editData');
	Route::get('orders', 'OrdersController@index');
	Route::post('orders', 'OrdersController@store');
	Route::get('orders/show/{id}', 'OrdersController@show');
	Route::get('orders/add-new', 'OrdersController@create');
	Route::get('orders/show/{order_id}', 'OrdersController@show');
	Route::get('orders/edit/{order_id}', 'OrdersController@edit');
	Route::get('orders/change-status/{order_id}/{status}', 'OrdersController@changeStatus');
	Route::get('orders/change-status-step/{step_id}/{status}', 'OrdersController@changeStatusStep');
	Route::get('orders/delete/{order_id}', 'OrdersController@destroy');
	Route::get('orders/get-services/{parent_id}/{order_id}', 'OrdersController@getServices');
	Route::get('orders/genenerate-file', 'OrdersController@generateFileWord');

	Route::get('orders/content-services', 'OrdersController@contentServices');
	Route::get('orders/generate-service-details/{ids}/{contact_id}/{order_id}', 'OrdersController@generateServiceDetails');
	Route::get('orders/download-file/{order_id}/{filename}', 'OrdersController@downloadFile');


	Route::post('orders/step-process/{order_id}', 'OrdersController@stepProcess');
	Route::post('orders/upload-scan/{order_id}', 'OrdersController@uploadScan');

	Route::post('orders/preview', 'OrdersController@preview');
	Route::post('postImageUpload', 'OrdersController@postImageUpload');

	Route::get('departments', 'DepartmentsController@index');


	Route::get('documents', 'DocumentsController@index');
	Route::get('documents/create-document-category', 'DocumentsController@createDocumentCategory');
	Route::get('documents/create-document', 'DocumentsController@create');
	Route::get('documents/get-documents', 'DocumentsController@getDocuments');
	Route::get('documents/download-documents/{id}', 'DocumentsController@downloadFile');
});