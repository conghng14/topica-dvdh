<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditServiceFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_fields', function(Blueprint $table)
        {
            $table->dropColumn('data_common_id');
            $table->dropColumn('title');
            $table->dropColumn('keyword');
            $table->dropColumn('type');
            $table->string('field_id')->after('service_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
