<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderCreateFieldXulyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table)
        {
            $table->string('member_shipcard')->nullable()->after('service_id');
            $table->string('shipping_status')->nullable()->after('service_id');
            $table->dateTime('shipping_date_success')->nullable()->after('service_id');
            $table->string('shipping_unit')->nullable()->after('service_id');
            $table->string('ma_vandon')->nullable()->after('service_id');
            $table->string('file_scan')->nullable()->after('service_id');
            $table->string('so_hop_dong')->nullable()->after('service_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
