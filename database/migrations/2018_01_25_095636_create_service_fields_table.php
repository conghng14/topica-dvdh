<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('data_common_id');
            $table->string('title')->nullable();
            $table->bigInteger('service_id')->nullable();
            $table->string('keyword')->nullable();
            $table->enum('type', ['text', 'select', 'editor', 'label'])->default('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_fields');
    }
}
