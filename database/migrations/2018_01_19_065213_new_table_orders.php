<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('contact_id')->nullable();
            $table->string('title')->nullable();
            $table->string('status')->default('NEW');
            $table->text('content')->nullable();
            $table->text('feedback')->nullable();
            $table->bigInteger('created_by_id');
            $table->bigInteger('received_by_id')->nullable();
            $table->timeStamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}