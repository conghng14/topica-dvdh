<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table)
        {
            $table->string('service_id')->nullable()->after('received_by_id');
            $table->string('service_name')->nullable()->after('received_by_id');
            $table->string('created_by_name')->nullable()->after('received_by_id');
            $table->string('received_by_name')->nullable()->after('received_by_id');
            $table->string('student_name')->nullable()->after('received_by_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
