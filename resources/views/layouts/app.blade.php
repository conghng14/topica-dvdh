<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('libs/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/nprogress/nprogress.css') }}" rel="stylesheet">
    @yield('css')
    <link href="{{ asset('libs/build/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <!-- <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><i class="fa fa-smile-o"></i> <span>Dashboard!</span></a>
                </div>

                <div class="clearfix"></div> -->

                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="{{ asset('imgs/mrBean.jpg') }}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome </span>
                        <h2>{{ auth()->user()->fullname }}</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /menu profile quick info -->

                <br/>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-exchange"></i> Quản lý Order<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    
                                    <li><a href="/orders/add-new">Tạo mới Order<span class="label label-success pull-right"></span></a></li>
                                    
                                    <li><a href="/orders?filter_status=PENDING">Danh sách order</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-files-o"></i>Tài liệu mẫu<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <!-- <li><a href="/documents/document-category">Tạo mới danh mục tài liệu</a></li> -->
                                    <!-- <li><a href="/documents/create-document">Tạo mới tài liệu</a></li> -->
                                    <li><a href="/documents">Danh sách tài liệu</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-newspaper-o"></i> Tin tức<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="form.html">Tạo mới Tin tức</a></li>
                                    <li><a href="form.html">Danh mục tin tức</a></li>
                                </ul>
                            </li>
                            <li><a href="/reports"><i class="fa fa-table"></i> Báo cáo thống kê</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="{{ asset('imgs/mrBean.jpg') }}" alt="">{{ auth()->user()->fullname }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out pull-right"></i>Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-bell-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="{{ asset('imgs/ngoc-trinh.jpg') }}" alt="Profile Image"/></span>
                                        <span>
                                            <span>Ngọc trinh</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Cạp đất ăn Bla bla Bla Bla bla Bla Bla bla Bla
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="{{ asset('imgs/ngoc-trinh.jpg') }}" alt="Profile Image"/></span>
                                        <span>
                                            <span>Ngọc trinh</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Cạp đất ăn Bla bla Bla Bla bla Bla Bla bla Bla
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="{{ asset('imgs/ngoc-trinh.jpg') }}" alt="Profile Image"/></span>
                                        <span>
                                            <span>Ngọc trinh</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Cạp đất ăn Bla bla Bla Bla bla Bla Bla bla Bla
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <div class="right_col" role="main">
            <div class="">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('libs/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('libs/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('libs/nprogress/nprogress.js') }}"></script>
<script src="{{ asset('libs/build/js/moment/moment.min.js') }}"></script>
<script src="{{ asset('libs/build/js/datepicker/daterangepicker.js') }}"></script>
<script>
    $(function(){
        $('#flash-overlay-modal').modal();
    });
</script>
@yield('js')

</body>
</html>
