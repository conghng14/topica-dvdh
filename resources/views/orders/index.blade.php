@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Danh sách Orders</h2>
                    <ul class="nav navbar-right panel_toolbox filter-list">
                        <li>
                            <input id="keyword" class="form-control" value="{{ $data_filter->keyword or '' }}" placeholder="Tìm kiếm..." />
                        </li>
                        <!-- <li>
                            <input type="text" name="" id="autocomplete-custom-append" class="form-control" placeholder="Lọc theo người tạo" />
                        </li> -->
                        <li>
                            <?php $filter_title = $data_filter->filter_status ? $data_filter->filter_status : '' ?>
                            <select class="form-control" id="filter_status" onchange="filterList()">
                                <option value="">Tất cả</option>
                                <option value="NEW" {{ $filter_title == 'NEW' ? 'selected' : '' }}>Order Mới</option>
                                <option value="RECEIVED" {{ $filter_title == 'RECEIVED' ? 'selected' : '' }}>Order đang xử lý</option>
                                <option value="PENDING" {{ $filter_title == 'PENDING' ? 'selected' : '' }}>Order đang chờ xử lý</option>
                                <option value="REJECTED" {{ $filter_title == 'REJECTED' ? 'selected' : '' }}>Order bị từ chối</option>                                
                                <option value="DONE" {{ $filter_title == 'DONE' ? 'selected' : '' }}>Order hoàn thành</option>
                            </select>
                        </li>
                        <li>
                            <button id="filter" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                     @include('flash::message')
                    <!-- start project list -->
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th style="width: 25%">Tên dịch vụ</th>
                            <th style="width: 10%">Người tạo</th>
                            <th style="width: 10%">Tên học viên</th>
                            <th>Người xử lý</th>
                            <th>Tiến trình</th>
                            <th>Trạng thái</th>
                            <th style="width: 12%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($orders) && $orders->count())
                                @foreach($orders as $k=>$order)
                                <tr class="rc_{{ $order->id }}">
                                    <td>{{ $k+1 }}</td>
                                    <td>
                                        {!! $order->warning !!}
                                        <a href="/orders/show/{{ $order->id }}">{{ $order->service_name }}</a>
                                        <br/>
                                        <small>Created {{ $order->created_at }}</small>
                                    </td>
                                    <td>{{ $order->created_by_name }}</td>
                                    <td>{{ $order->student_name }}</td>
                                    <td>{{ $order->received_by_name or "" }}</td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar"
                                                 data-transitiongoal="{{ $order->progress_bar }}"></div>
                                        </div>
                                        <small>{{ $order->progress_bar }}% Complete</small>
                                    </td>
                                    <td>
                                        <?php
                                        $status = "";
                                        switch ($order->status) {
                                            case "NEW":
                                                $status = "Mới tạo";
                                                break;
                                            case "PENDING":
                                                $status = "Đang chờ xử lý";
                                                break;
                                            case "RECEIVED":
                                                $status = "Đang xử lý";
                                                break;
                                            case "REJECTED":
                                                $status = "BỊ từ chối";
                                                break;
                                            case "DONE":
                                                $status = "Đã hoàn thành";
                                                break;
                                            default:
                                                $status = $status;
                                        }
                                        ?>
                                        <button type="button" class="btn btn-success btn-xs {{ $order->id }}">{{ $status }}</button>
                                        @if($order->status == 'NEW')
                                        <button type="button" class="btn btn-primary btn-xs send_{{ $order->id }}" onclick="changeStatus({{ $order->id }}, 'PENDING')">Gửi</button>
                                        @endif
                                        <!--<button type="button" class="btn btn-primary btn-xs" onclick="changeStatus({{ $order->id }}, 'NEW')">Hủy Order</button>-->
                                    </td>
                                    <td>
                                        <a href="/orders/show/{{ $order->id }}" class="btn btn-primary btn-xs" 
                                            title="Xem chi tiết"><i class="fa fa-eye"></i></a>
                                        <a href="/orders/edit/{{ $order->id }}" class="btn btn-primary btn-xs" 
                                            {{ $order->created_by_id == auth()->user()->id ? '' : 'disabled' }} 
                                            class="btn btn-info btn-xs" title="Sửa order"><i class="fa fa-pencil"></i></a>
                                        @if($order->status == 'DONE' || $order->created_by_id != auth()->user()->id)
                                            <a href="#" class="btn btn-danger btn-xs" title="Xóa order" disabled ><i class="fa fa-trash-o"></i></a>
                                        @else
                                            <a href="#" class="btn btn-danger btn-xs" title="Xóa order" onclick="deleteRecord({{ $order->id }})"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                        
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">There are no data.</td>
                                </tr>
                            @endif                      
                        </tbody>
                    </table>
                    <!-- end project list -->
                    {{ $orders->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    @parent

    <script src="{{ asset('libs/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('libs/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('libs/build/js/custom.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(function(){
            $('#datatable').dataTable({
              "pageLength": 14,
              "lengthChange" : false
            });
            $("#keyword").keyup(function (e) {
                if (e.keyCode == 13) {
                    filterList();
                }
            });
        });
        $("#filter").on('click', function(){
            filterList();
        });

        function filterList(){
            var keyword = $("#keyword").val();
            var filter_status = $("#filter_status").val();
            window.location.href = "{{ url('/orders' ) }}?filter_status=" + filter_status + "&keyword=" + keyword;
        }
        function deleteRecord(order_id){
            if(confirm("Bạn có chắc chắn xóa?")){
                $.get("/orders/delete/"+ order_id , function(status){
                    $('.rc_'+ order_id).hide();
                });
            }
        }
    </script>
@endsection
