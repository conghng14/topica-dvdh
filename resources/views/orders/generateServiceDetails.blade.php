<div id="service_{{ $service->id }}">
	<h4 class="service_type">{{ $service->title }}</h4>
	<?php $i=0;?>
	@foreach($service->service_fields as $child)
		@if($child->field)
			@if($child->field->type == 'label')
				<div class="clear"></div>
				<h5><span>{{ $child->field->title or '' }}</span></h5>
			@else
				<?php 
					if(count($order_service_data) > 0){
						$val = isset($order_service_data[$child->field->keyword]) ? $order_service_data[$child->field->keyword] : '';
					}else{
						$val = isset($contact_data[$child->field->keyword]) ? $contact_data[$child->field->keyword] : '';
					}
				?>
				@if($child->field->keyword == 'contact_id')
					<input class="col-md-5 col-sm-12 col-xs-12" type="hidden" name="service_data[{{$service->id}}][{{ $child->field->keyword }}]" 
					value="{{ $contact_id ? $contact_id : $val }}" />

				@else
					<div class="form-group col-md-6 col-sm-12 col-xs-12 ">
					    <label class="col-md-5 col-sm-12 col-xs-12" >
					    	{{ $child->field->title or '' }}
							{{ $child->rq == 1 ? '*' : '' }}
					    </label>
					    @if($child->field->type == 'select')
					    	@if($child->field->keyword != "tang_pham")
					    		<select data-parsley-errors-messages-disabled class="col-md-5 col-sm-12 col-xs-12 {{ $child->field->keyword }}" {{ $child->rq == 1 ? 'required' : '' }} name="service_data[{{$service->id}}][{{ $child->field->keyword }}]">
					                @foreach($child->field->data_select as $rc)
					                    <option {{ $val == $rc->title ? 'selected' : '' }} value="{{ $rc->title }}">{{ $rc->title }}</option>
					                @endforeach
					            </select>
					    	@else
					    		<select multiple="multiple" class="select2_multi col-md-5 col-sm-12 col-xs-12 {{ $child->field->keyword }}" {{ $child->rq == 1 ? 'required' : '' }} 
					    			name="service_data[{{$service->id}}][{{ $child->field->keyword }}][]">
					                @foreach($child->field->data_select as $rc)
					                	<?php
					                	$selected = "";
					                	if(is_array($val)){
					                		if(in_array($rc->title, $val)){
						                		$selected = "selected";
						                	} 
					                	}					                	
					                	?>
					                    <option {{ $selected }} value="{{ $rc->title }}">{{ $rc->title }}</option>
					                @endforeach
					            </select>
					            @section('css')
								@endsection
					    	@endif
							
						@elseif($child->field->type == 'text')
							<input class="col-md-5 col-sm-12 col-xs-12 {{ $child->field->keyword }}" type="text" data-parsley-errors-messages-disabled {{ $child->rq == 1 ? 'required' : '' }} name="service_data[{{$service->id}}][{{ $child->field->keyword }}]" 
								value="{{ $val }}" />
						@elseif($child->field->type == 'number')
							<input class="col-md-5 col-sm-12 col-xs-12 {{ $child->field->keyword }}" type="text" data-parsley-errors-messages-disabled {{ $child->rq == 1 ? 'required' : '' }} name="service_data[{{$service->id}}][{{ $child->field->keyword }}]" 
								data-inputmask="'alias': 'numeric'" value="{{ $val }}" />
						@elseif($child->field->type == 'money')
							<input class="col-md-5 col-sm-12 col-xs-12 {{ $child->field->keyword }}" type="text" data-parsley-errors-messages-disabled {{ $child->rq == 1 ? 'required' : '' }} name="service_data[{{$service->id}}][{{ $child->field->keyword }}]" 
								data-inputmask="'alias': 'currency', 'prefix' : '', 'placeholder':'', 'digits':'*', 'digitsOptional':'true'" value="{{ $val }}" />				
						@elseif($child->field->type == 'date')
							<input data-inputmask="'mask': '99/99/9999'" data-parsley-errors-messages-disabled {{ $child->rq == 1 ? 'required' : '' }} placeholder="dd/mm/yyyy" class="col-md-5 col-sm-12 col-xs-12" type="text" name="service_data[{{$service->id}}][{{ $child->field->keyword }}]" 
							value="{{ $val }}" />
						@elseif($child->field->type == 'editor')
							<textarea class="col-md-5 col-sm-12 col-xs-12" name="service_data[{{$service->id}}][{{ $child->field->keyword }}]" data-parsley-errors-messages-disabled {{ $child->rq == 1 ? 'required' : '' }} row="3" >{{ $val }}</textarea>
						@endif
					</div>		
					<?php $i++;?>	
				@endif	
				@if($i % 2 ==0 && $i != 0)
				<div class="clear"></div>
				@endif		
			@endif
		@endif			
	@endforeach
	@if($service->type == "view_form_with_upload_file")
	<div class="clear"></div>
	<div class="form-group col-md-6 col-sm-12 col-xs-12 ">
	    <label class="col-md-5 col-sm-12 col-xs-12" >
	    	Tài liệu đính kèm:
	    </label>
	    <input type="file" name="upload_file" data-parsley-max-file-size="5000" 
	    	data-parsley-filemimetypes="application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
	</div>
	@endif	
</div>