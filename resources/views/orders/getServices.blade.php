@foreach($services as $service)
    <option value="{{ $service->id }}" {{ in_array($service->id, $orderServices) ? "selected" : "" }} >{{ $service->title }}</option>
@endforeach