@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $order->title }}</h2>
                    <div class="clearfix"></div>
                </div>
                @include('flash::message')
                <div class="x_content order-details">
                    <div class="row">
                        <div class="col-sm-12 mail_view">
                            <div class="inbox-body">
                                <div class="mail_heading row">
                                    <div class="col-md-6">
                                        <div class="btn-group" style="margin-bottom:10px">
                                            <button class="btn btn-sm btn-default" type="button">
                                                Trạng thái: {{ $order->status_name or '' }}
                                            </button>
                                            @if(($order->status == 'NEW' && auth()->user()->id == $order->created_by_id) || $order->status == 'REJECTED' && auth()->user()->id == $order->created_by_id)
                                            <?php
                                            $status = $order->status == 'REJECTED' ? "RECEIVED" : "PENDING";
                                            ?>
                                            <button class="btn btn-sm btn-default send_{{ $order->id or '' }}" type="button" onclick="changeStatus({{ $order->id }}, '{{ $status }}')">
                                                <i class="fa fa-send-o"></i> Gửi
                                            </button>
                                            @endif
                                            @if($order->status != 'DONE' && $order->status != 'NEW' && auth()->user()->id == $order->created_by_id)
                                            <button class="btn btn-sm btn-default send_{{ $order->id or '' }}" type="button" onclick="changeStatus({{ $order->id }}, 'NEW')">
                                                <i class="fa fa-arrow-circle-o-left"></i> Hủy Order
                                            </button>
                                            @endif
                                            @if($order->created_by_id === auth()->user()->id)
                                                <button class="btn btn-sm btn-default send_{{ $order->id or '' }}" type="button" onclick="deleteRecord({{ $order->id }})">
                                                    <i class="fa fa-times"></i> Xóa order
                                                </button>
                                            @endif
                                            
                                            @if($order->status != 'DONE' && $order->status != 'NEW' && $order->status != 'REJECTED' && auth()->user()->id == $order->received_by_id)
                                            <button class="btn btn-sm btn-default" type="button" data-toggle="modal" data-target="#rejected" >
                                                <i class="fa fa-minus-square-o"></i> Từ chối
                                            </button>
                                            @endif
                                            

                                            @if($order->status == 'PENDING' && $order->received_by_id == null && 
                                                (userCan('handle-order') || userCan('assign-order') || auth()->user()->role_id == 1))
                                            <button class="btn btn-sm btn-default btnReceived" type="button" 
                                                onclick="changeStatus({{ $order->id }}, 'RECEIVED')">
                                                <i class="fa fa-download"></i> Nhận Order
                                            </button>
                                            @endif
                                            <!-- @if($order->received_by_id === auth()->user()->id)
                                                @if($order->status == 'RECEIVED')
                                                <button class="btn btn-sm btn-default" type="button" onclick="changeStatus({{ $order->id }}, 'DONE')">
                                                    <i class="fa fa-check-square-o"></i> Hoàn thành Order
                                                </button>
                                                @endif
                                            @endif -->
                                        </div>
                                            
                                        <div class="sender-info">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>{{ $order->created_by->full_name }}</strong>
                                                    <span>({{ $order->created_by->email }})</span> to
                                                    <strong>{{ $order->received_by->email or "N/A" }}</strong>                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <p class="date">Ngày: {{ $order->created_at }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        @if($order->received_by_id === auth()->user()->id)
                                            @if($order->status == 'RECEIVED')
                                            <button type="button" class="btn btn-primary btn-lg" onclick="changeStatus({{ $order->id }}, 'DONE')">Hoàn thành Order</button>
                                            @endif
                                        @endif
                                        
                                        <!-- @if($order->steps->count() > 0)
                                        <h2 style="margin-top:0px;margin-bottom:5px">Quy trình xử lý:</h2>
                                        <table style="float:left; width:300px;border-right:1px solid #cecece">
                                            @foreach($order->steps as $k=>$step)
                                            <tr>
                                                <td>
                                                    @if($k == 2)
                                                    <button id="send" type="submit" class="btn btn-success btn-xs">Xử lý</button>
                                                    @else
                                                    <button id="send" type="submit" class="btn btn-default btn-xs">Đã hoàn thành</button>
                                                    @endif
                                                    
                                                </td>
                                                <td>{{ $step->title }}
                                                    @if($step->is_cod == 1)
                                                    - {{ $step->bill_status }}
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach   
                                        </table>                              
                                        @endif
                                        <div style="float:right;width:280px;min-height:100px;">123</div> -->
                                    </div>
                                </div>
                                <hr style="margin:5px 0px" />
                                <div class="view-mail">
                                    @if($order->steps->count() > 0)
                                    <h4 class="service_type">Quy trình xử lý</h4>
                                    <div class="qtxl">
                                        @foreach($order->steps as $step)
                                            @if($step->keyword != null)
                                                <?php
                                                $disabled = "";
                                                if(auth()->user()->id != $order->received_by_id || $order->status != "RECEIVED"){
                                                    $disabled = "disabled ";
                                                }
                                                if($step->is_cod == 1 && $step->bill_status != "DONE"){
                                                    //$disabled = "disabled ";
                                                }
                                                ?>

                                                @if($step->keyword == "in-hop-dong")
                                                    <div class="list-qtxl form-group col-md-2 col-sm-12 col-xs-12 ">
                                                        <label class="col-md-12 col-sm-12 col-xs-12" >
                                                            <input type="checkbox" value="{{ $step->id }}" {{ $disabled }} class="flat flat_{{ $step->id }}" {{ $step->status == 1 ? "checked" : "" }} /> 
                                                            {{ $step->title }}                                             
                                                        </label>
                                                        <!-- <div class="col-md-12 col-sm-12 col-xs-12">123</div> -->
                                                    </div>
                                                @elseif($step->keyword == "xin-chu-ky")
                                                    <div class="list-qtxl form-group col-md-3 col-sm-12 col-xs-12 ">
                                                        <label class="col-md-12 col-sm-12 col-xs-12" >
                                                            <input type="checkbox" value="" class="flat flat_" disabled {{ $step->status == 1 ? "checked" : "" }} /> 
                                                            {{ $step->title }}
                                                        </label>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            Số hợp đồng
                                                            <input class="form-control col-md-7 col-xs-12" {{ $disabled }} id="so_hd" value="{{ $order->so_hop_dong }}" placeholder="Số hợp đồng" type="text" />
                                                            @if($order->status == 'RECEIVED')
                                                            <div class="clearfix"></div><br />
                                                            <button type="button" class="btn btn-default btn-xs" onclick="stepProcess('ud_so_hd')">Cập nhật</button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @elseif($step->keyword == "scan-hop-dong")
                                                    <div class="list-qtxl form-group col-md-3 col-sm-12 col-xs-12 ">
                                                        <label class="col-md-12 col-sm-12 col-xs-12" >
                                                            <input type="checkbox" value="" class="flat flat_" disabled {{ $step->status == 1 ? "checked" : "" }} /> 
                                                            {{ $step->title }}
                                                        </label>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            Upload bản scan
                                                            <form enctype="multipart/form-data" role="form" method="POST" action="/orders/upload-scan/{{ $order->id }}" >
                                                                @if($order->status == 'RECEIVED')
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input type="file" name="upload_file" />
                                                                @endif
                                                                @if($order->file_scan != null)
                                                                    <div class="clearfix"></div>
                                                                    <span><i class="fa fa-file-word-o"></i> {{ $order->file_scan }} | </span>
                                                                    <a href="/orders/download-file/{{ $order->id }}/{{ $order->file_scan }}">Download</a>
                                                                @endif
                                                                @if($order->status == 'RECEIVED')
                                                                <div class="clearfix"></div><br />
                                                                <button type="submit" class="btn btn-default btn-xs">Cập nhật</button>
                                                                @endif
                                                            </form>
                                                        </div>
                                                    </div>
                                                @elseif($step->keyword == "gui")
                                                    <div class="list-qtxl form-group col-md-3 col-sm-12 col-xs-12 ">
                                                        <label class="col-md-12 col-sm-12 col-xs-12" >
                                                            <input type="checkbox" value="" class="flat flat_" disabled {{ $step->status == 1 ? "checked" : "" }} /> 
                                                            {{ $step->title }}
                                                        </label>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            Đơn vị vận chuyển
                                                            <select class="form-control" id="don_vi_vc" required="required" {{ $disabled }}>
                                                                <option value="">Chọn đơn vị vận chuyển</option>
                                                                <option value="Viet-air" {{ $order->shipping_unit == "Viet-air" ? "selected" : "" }}>Viet-air</option>
                                                                <option value="Viettel miền bắc" {{ $order->shipping_unit == "Viettel miền bắc" ? "selected" : "" }}>Viettel miền bắc</option>
                                                                <option value="Viettel miền nam" {{ $order->shipping_unit == "Viettel miền nam" ? "selected" : "" }}>Viettel miền nam</option>
                                                                <option value="Giao hàng tiết kiệm" {{ $order->shipping_unit == "Giao hàng tiết kiệm" ? "selected" : "" }}>Giao hàng tiết kiệm</option>
                                                                <option value="TVTS" {{ $order->shipping_unit == "TVTS" ? "selected" : "" }}>TVTS</option>
                                                            </select>
                                                            <div class="clearfix"></div>
                                                            Mã vận đơn:
                                                            <input class="form-control col-md-7 col-xs-12" {{ $disabled }} id="ma_vandon" value="{{ $order->ma_vandon }}" placeholder="Mã vận đơn" type="text" />
                                                            <div class="clearfix"></div>                                                           
                                                            Dự kiến ngày phát thành công:
                                                            <b>{{ $order->shipping_date_success }}</b>
                                                            <a href="#" data-toggle="modal" data-target="#shippingDetails">Tình trạng vận chuyển: <i class="fa fa-search"></i></a>
                                                            <input class="form-control col-md-7 col-xs-12" {{ $disabled }} id="shipping_status" value="{{ $order->shipping_status }}" placeholder="Tình trạng" type="text" />
                                                            <div class="clearfix"></div>
                                                            (Nhập đúng chuỗi "Hoàn thành" nếu đã chuyển thành công)
                                                            @if($order->status == 'RECEIVED')
                                                            <div class="clearfix"></div><br />
                                                            <button type="button" class="btn btn-default btn-xs" onclick="stepProcess('ud_dvvc')">Cập nhật</button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @elseif($step->keyword == "cod-thutien")
                                                    <div class="list-qtxl form-group col-md-3 col-sm-12 col-xs-12 ">
                                                        <label class="col-md-12 col-sm-12 col-xs-12" >
                                                            <input type="checkbox" value="" value="{{ $order->member_shipcard }}" class="flat flat_" disabled {{ $step->status == 1 ? "checked" : "" }} /> 
                                                            {{ $step->title }}
                                                        </label>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            Cập nhật Member shipcard
                                                            <input class="form-control col-md-7 col-xs-12" id="cod-thutien" placeholder="Member shipcard" type="text" value="{{ $order->member_shipcard }}" />
                                                            @if($order->status == 'RECEIVED')
                                                            <div class="clearfix"></div><br />
                                                            <button type="button" class="btn btn-default btn-xs" onclick="stepProcess('member_shipcard')">Cập nhật</button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    </div>                                    
                                    @endif
                                    </div>
                                    @foreach($order->orderService as $data)
                                        <div class="clear"></div>
                                        <h4 class="service_type">{{ $data->service->title }}
                                            @if(($data->service->type == "view_file" || $data->service->type == "view_form_with_upload_file") && $data->file_path != null)
                                                <div class="attachment">
                                                    <p>
                                                        <span><i class="fa fa-file-word-o"></i> {{ $data->file_path }} | </span>
                                                        <a href="/orders/download-file/{{ $order->id }}/{{ $data->file_path }}">Download</a>
                                                        @if($data->service->type != "view_form_with_upload_file")
                                                         -
                                                        <a href="#" data-toggle="modal" data-target="#preview">View</a>
                                                        @endif
                                                    </p>
                                                </div>
                                            @endif
                                        </h4>
                                        @foreach($data->orderServiceData as $k=>$dt)                                            
                                            <div class="list-details form-group col-md-6 col-sm-12 col-xs-12 ">
                                                <label class="col-md-5 col-sm-12 col-xs-12" >{{ $dt->field_name or '' }}</label>
                                                <div class="col-md-5 col-sm-12 col-xs-12">{{ $dt->data }}</div>
                                            </div>
                                        @endforeach
                                        
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div id="preview" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Xem trước tài liệu</h4>
        </div>
        <div class="modal-body">
            <?php
                $file_view = url("/storage/documents/" . $order->created_by_id . "/" . $data->file_path);
            ?>
          <iframe style="width:100%;height:800px" src="https://docs.google.com/gview?url={{ $file_view }}&embedded=true" frameborder="0">
            </iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>

      </div>
    </div>
</div>

<div id="rejected" class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Lý do từ chối order</h4>
        </div>
        <div class="modal-body">
            <textarea id="msg_rejected" rows="8" class="form-control" name="msg_rejected" ></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>

      </div>
    </div>
</div>

<div id="shippingDetails" class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Chi tiết</h4>
        </div>
        <div class="modal-body">
            {!! $order->shipping_status_details !!}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
</div>
@endsection
@section('css')
<link href="{{ asset('libs/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
@endsection

@section('js')
    @parent

    <script src="{{ asset('libs/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('libs/validator/validator.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("input.flat")[0] && $(document).ready(function() {
                $("input.flat").iCheck({
                    checkboxClass: "icheckbox_minimal-blue"
                })
            });
            $('input.flat').on('ifChanged', function(event){
                var checkboxChecked = $(this).is(':checked');

                if(checkboxChecked) {
                    var status = 1;
                    changeStatusStep($(this).val(), status);;
                }else{
                    var status = 0;
                    changeStatusStep($(this).val(), status);
                }           
            });
            
        });

        function changeStatusStep(step_id, status){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: 'application/json; charset=utf-8',
                type: 'GET',  
                url: "/orders/change-status-step/" + step_id + "/"+ status,
                context: {}
            }).done(function(data) {
                if(data == 2){
                    alert('Bạn không phải người xử lý order này!');
                    return false;
                }
                // if(data == 3){
                //     alert('COD chưa gửi thành công!');
                //     return false;
                // }
            });
        }

        $(document).ready(function() {
            $('#a_birthday').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_2"
            });
        });
        $("#preview").on('click', function(){
            a_name = $("#a_name").val();
            a_birthday = $("#a_birthday").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: 'application/json; charset=utf-8',
                type: 'GET',  
                url: "/orders/template-preview?a_name=" + a_name + "&a_birthday="+ a_birthday,
                context: {}
            }).done(function(data) {
                $('#preview .modal-body').html(data);
                $('#preview .modal').modal('toggle');
            });
        });
        // initialize the validator function
        validator.message.date = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });
        function deleteRecord(order_id){
            if(confirm("Bạn có chắc chắn xóa?")){
                $.get("/orders/delete/"+ order_id , function(status){
                    $('.rc_'+ order_id).hide();
                });
            }
        }

        function stepProcess(type){
            if(type == 'ud_so_hd'){
                data = $("#so_hd").val();
            }else if(type == 'ud_dvvc'){
                data = $("#don_vi_vc").val();
            }else if(type == 'member_shipcard'){
                data = $("#cod-thutien").val();
            }else{
                data = '';
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',  
                url: "/orders/step-process/{{ $order->id }}",
                data: {
                    type: type,
                    data: data,
                    ma_vandon: $('#ma_vandon').val(),
                    shipping_status: $('#shipping_status').val()
                }
            }).done(function(data) {
                location.reload();
            });
        }
    </script>
    <script src="{{ asset('libs/build/js/custom.min.js') }}"></script>
@endsection
