@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ isset($order) ? "Sửa order" : "Tạo mới order" }}</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" data-parsley-validate id="submit_order" action="/orders" method="POST" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $order->id or '' }}">
                        <input type="hidden" name="prview" id="prview" value="0">
                        <div class="item form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title">Tiêu đề <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="title" class="form-control col-md-7 col-xs-12" name="title" placeholder=""
                                       required="required" data-parsley-errors-messages-disabled type="text" value="{{ $order->title or '' }}" />
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title">Contact ID
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="contact_id" class="form-control col-md-7 col-xs-12" name="contact_id" placeholder=""
                                    type="text" value="{{ $order->contact_id or '' }}" />
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Chọn sản phẩm<span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="cat" required="required" id="cat_id" >
                                    @foreach($services as $service)
                                        <option value="{{ $service->id }}" {{ in_array($service->id, $orderServices) ? "selected" : "" }} >{{ $service->title }}</option>
                                    @endforeach                                    
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Chọn loại dịch vụ <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_multiple select2_group form-control list_services" name="services[]" required="required" multiple="multiple">
                                </select>
                            </div>
                        </div>
                        <hr />
                        <div class="x_title">
                            <h2>Thông tin dịch vụ</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row temp_content">
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-7 col-md-offset-3">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button id="send" type="submit" class="btn btn-default">Lưu order</button>
                                <button id="preview" type="button" class="btn btn-default" target="_newtab">Xem trước tài liệu</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
<link href="{{ asset('libs/select2/dist/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    @parent

    <script src="{{ asset('libs/parsleyjs/dist/parsley.min.js') }}"></script>
    <script src="{{ asset('libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('libs/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('js/processNumber.js') }}"></script>
    
    <!-- <script src="{{ asset('libs/tinymce/tinymce.min.js') }}"></script> -->
    <script>
        $(function () {
           // var t = numberToString("1,200");
            //alert(t);
            getServices({{ $order->id or '0' }});
            //createUploader('my_uploader_id'); // The Id that you used to create with the builder
            // tinymce.init({
            //     selector: '#tinnymce',
            //     menubar: false,
            //     plugins: ['code preview link table'],
            //     toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | table bullist link preview'
            // });
            @if(count($orderServices) > 0)
                @for($i=0; $i< count($orderServices); $i++)
                    generateServiceDetails({{ $orderServices[$i] }}, {{ $order->id or '' }});
                @endfor
            @endif

            $('#a_birthday').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_2"
            });
            $(".select2_multiple").select2({
              maximumSelectionLength: 2,
              placeholder: "Chọn một hoặc nhiều dịch vụ",
              allowClear: true
            });
            $('.select2_multiple').on("select2:select", function(e) { 
                var id_select = e.params.data.id;
                generateServiceDetails(id_select);
            });
            $('.select2_multiple').on("select2:unselect", function(e) {
                var id_unselect = e.params.data.id;
                $('#service_' + id_unselect).remove()
            });
        });


            $( "#cat_id" ).change(function() {
                getServices({{ $order->id or '0' }});
            });
        
        function generateServiceDetails(id, order_id = 0){
            var contact_id = $("#contact_id").val();
                if(!contact_id) {
                    contact_id = 0;
                }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',  
                url: "/orders/generate-service-details/"+ id + "/" + contact_id + "/" + order_id,
                context: {}
            }).done(function(data) {
                $('.temp_content').append(data);
                $(":input").inputmask();

                var thoi_gian_dao_tao = $('select.thoi_gian_dao_tao').val();
                $('.thoi_gian_dao_tao_bang_chu').val(numberToString(thoi_gian_dao_tao));

                var thoi_gian_bao_luu = $('select.thoi_gian_bao_luu').val();
                $('.thoi_gian_bao_luu_bang_chu').val(numberToString(thoi_gian_bao_luu));

                var so_lan_bao_luu = $('select.so_lan_bao_luu').val();
                $('.so_lan_bao_luu_bang_chu').val(numberToString(so_lan_bao_luu));

                var hoc_phi_goc = $('select.hoc_phi_goc').val();
                if(hoc_phi_goc){
                    hoc_phi_goc = hoc_phi_goc.replace(new RegExp(",", "g"), '');    
                    $('.hoc_phi_goc_bang_chu').val(numberToCurrency(hoc_phi_goc));    
                }                

                $.getScript("{{ asset('libs/select2/dist/js/select2.full.min.js') }}");
  
                $(".select2_multi").select2({
                    maximumSelectionLength: 10,
                    placeholder: "Chọn một hoặc nhiều tặng phẩm",
                    allowClear: true
                });

            });
        }

        function getServices(order_id){
            var parent_id = $('#cat_id').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',  
                url: "/orders/get-services/"+ parent_id + "/" + order_id,
                context: {}
            }).done(function(data) {
                $('.list_services').html(data);
                $('.temp_content').html('');
            });
        }

        $("#preview").on('click', function(){
            $('#submit_order').attr('target', '_blank');
            $('#submit_order').attr('action', '/orders/preview');
            $('#submit_order').submit();
        });
        $("#send").on('click', function(){            
            $('#submit_order').removeAttr('target');
            $('#submit_order').attr('action', '/orders');
            $('#submit_order').submit();
        });
        // initialize the validator function
        //validator.message.date = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        // $('form')
        //     .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        //     .on('change', 'select.required', validator.checkField)
        //     .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        window.Parsley.addValidator('maxFileSize', {
            validateString: function(_value, maxSize, parsleyInstance) {
                if (!window.FormData) {
                  alert('You are making all developpers in the world cringe. Upgrade your browser!');
                  return true;
                }
                var files = parsleyInstance.$element[0].files;
                return files.length != 1  || files[0].size <= maxSize * 1024;
            },
            requirementType: 'integer',
            messages: {
                en: 'Dung lượng file không được lớn hơn %s Kb.'
            }
        });

        window.Parsley.addValidator('filemimetypes', {
            validateString: function (_value, requirement, parsleyInstance) {
                if (!window.FormData) {
                    alert('You are making all developpers in the world cringe. Upgrade your browser!');
                    return true;
                }
                var file = parsleyInstance.$element[0].files;
                if (file.length == 0) {
                    return true;
                }
                console.log(file[0].type);
                var allowedMimeTypes = requirement.replace(/\s/g, "").split(',');
                return allowedMimeTypes.indexOf(file[0].type) !== -1;

            },
            requirementType: 'string',
            messages: {
                en: 'Chỉ chấp nhận file word (docx, doc) và excel (xlsx, xls).'
            }
        });

        
    </script>
    <script src="{{ asset('libs/build/js/custom.min.js') }}"></script>
@endsection
