﻿<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
    <style type="text/css">
	body{font-family: 'Arial'}
    ol.lst-kix_list_1-3 {
        list-style-type: none
    }

    ol.lst-kix_list_1-4 {
        list-style-type: none
    }

    .lst-kix_list_2-6 > li:before {
        content: "" counter(lst-ctn-kix_list_2-6, lower-latin) ". "
    }

    .lst-kix_list_2-7 > li:before {
        content: "" counter(lst-ctn-kix_list_2-7, lower-roman) ". "
    }

    .lst-kix_list_2-7 > li {
        counter-increment: lst-ctn-kix_list_2-7
    }

    ol.lst-kix_list_1-5 {
        list-style-type: none
    }

    ol.lst-kix_list_1-6 {
        list-style-type: none
    }

    .lst-kix_list_2-1 > li {
        counter-increment: lst-ctn-kix_list_2-1
    }

    ol.lst-kix_list_1-0 {
        list-style-type: none
    }

    .lst-kix_list_2-4 > li:before {
        content: "" counter(lst-ctn-kix_list_2-4, lower-roman) ". "
    }

    .lst-kix_list_2-5 > li:before {
        content: "" counter(lst-ctn-kix_list_2-5, decimal) ". "
    }

    .lst-kix_list_2-8 > li:before {
        content: "" counter(lst-ctn-kix_list_2-8, decimal) ". "
    }

    ol.lst-kix_list_1-1 {
        list-style-type: none
    }

    ol.lst-kix_list_1-2 {
        list-style-type: none
    }

    .lst-kix_list_1-1 > li {
        counter-increment: lst-ctn-kix_list_1-1
    }

    ol.lst-kix_list_2-6.start {
        counter-reset: lst-ctn-kix_list_2-6 0
    }

    ol.lst-kix_list_1-8.start {
        counter-reset: lst-ctn-kix_list_1-8 0
    }

    ol.lst-kix_list_2-3.start {
        counter-reset: lst-ctn-kix_list_2-3 0
    }

    ol.lst-kix_list_1-5.start {
        counter-reset: lst-ctn-kix_list_1-5 0
    }

    ol.lst-kix_list_1-7 {
        list-style-type: none
    }

    .lst-kix_list_1-7 > li {
        counter-increment: lst-ctn-kix_list_1-7
    }

    ol.lst-kix_list_1-8 {
        list-style-type: none
    }

    ol.lst-kix_list_2-5.start {
        counter-reset: lst-ctn-kix_list_2-5 0
    }

    .lst-kix_list_2-0 > li {
        counter-increment: lst-ctn-kix_list_2-0
    }

    .lst-kix_list_2-3 > li {
        counter-increment: lst-ctn-kix_list_2-3
    }

    .lst-kix_list_2-6 > li {
        counter-increment: lst-ctn-kix_list_2-6
    }

    ol.lst-kix_list_1-7.start {
        counter-reset: lst-ctn-kix_list_1-7 0
    }

    .lst-kix_list_1-2 > li {
        counter-increment: lst-ctn-kix_list_1-2
    }

    ol.lst-kix_list_2-2.start {
        counter-reset: lst-ctn-kix_list_2-2 0
    }

    .lst-kix_list_1-5 > li {
        counter-increment: lst-ctn-kix_list_1-5
    }

    .lst-kix_list_1-8 > li {
        counter-increment: lst-ctn-kix_list_1-8
    }

    ol.lst-kix_list_1-4.start {
        counter-reset: lst-ctn-kix_list_1-4 0
    }

    ol.lst-kix_list_1-1.start {
        counter-reset: lst-ctn-kix_list_1-1 0
    }

    ol.lst-kix_list_2-2 {
        list-style-type: none
    }

    ol.lst-kix_list_2-3 {
        list-style-type: none
    }

    ol.lst-kix_list_2-4 {
        list-style-type: none
    }

    ol.lst-kix_list_2-5 {
        list-style-type: none
    }

    .lst-kix_list_1-4 > li {
        counter-increment: lst-ctn-kix_list_1-4
    }

    ol.lst-kix_list_2-0 {
        list-style-type: none
    }

    .lst-kix_list_2-4 > li {
        counter-increment: lst-ctn-kix_list_2-4
    }

    ol.lst-kix_list_1-6.start {
        counter-reset: lst-ctn-kix_list_1-6 0
    }

    ol.lst-kix_list_2-1 {
        list-style-type: none
    }

    ol.lst-kix_list_1-3.start {
        counter-reset: lst-ctn-kix_list_1-3 0
    }

    ol.lst-kix_list_2-8.start {
        counter-reset: lst-ctn-kix_list_2-8 0
    }

    ol.lst-kix_list_1-2.start {
        counter-reset: lst-ctn-kix_list_1-2 0
    }

    .lst-kix_list_1-0 > li:before {
        content: "" counter(lst-ctn-kix_list_1-0, decimal) ". "
    }

    ol.lst-kix_list_2-6 {
        list-style-type: none
    }

    .lst-kix_list_1-1 > li:before {
        content: "" counter(lst-ctn-kix_list_1-1, lower-latin) ". "
    }

    .lst-kix_list_1-2 > li:before {
        content: "" counter(lst-ctn-kix_list_1-2, lower-roman) ". "
    }

    ol.lst-kix_list_2-0.start {
        counter-reset: lst-ctn-kix_list_2-0 0
    }

    ol.lst-kix_list_2-7 {
        list-style-type: none
    }

    ol.lst-kix_list_2-8 {
        list-style-type: none
    }

    .lst-kix_list_1-3 > li:before {
        content: "" counter(lst-ctn-kix_list_1-3, decimal) ". "
    }

    .lst-kix_list_1-4 > li:before {
        content: "" counter(lst-ctn-kix_list_1-4, lower-latin) ". "
    }

    ol.lst-kix_list_1-0.start {
        counter-reset: lst-ctn-kix_list_1-0 0
    }

    .lst-kix_list_1-0 > li {
        counter-increment: lst-ctn-kix_list_1-0
    }

    .lst-kix_list_1-6 > li {
        counter-increment: lst-ctn-kix_list_1-6
    }

    .lst-kix_list_1-7 > li:before {
        content: "" counter(lst-ctn-kix_list_1-7, lower-latin) ". "
    }

    ol.lst-kix_list_2-7.start {
        counter-reset: lst-ctn-kix_list_2-7 0
    }

    .lst-kix_list_1-3 > li {
        counter-increment: lst-ctn-kix_list_1-3
    }

    .lst-kix_list_1-5 > li:before {
        content: "" counter(lst-ctn-kix_list_1-5, lower-roman) ". "
    }

    .lst-kix_list_1-6 > li:before {
        content: "" counter(lst-ctn-kix_list_1-6, decimal) ". "
    }

    .lst-kix_list_2-0 > li:before {
        content: "" counter(lst-ctn-kix_list_2-0, lower-latin) ". "
    }

    .lst-kix_list_2-1 > li:before {
        content: "" counter(lst-ctn-kix_list_2-1, lower-roman) ". "
    }

    ol.lst-kix_list_2-1.start {
        counter-reset: lst-ctn-kix_list_2-1 0
    }

    .lst-kix_list_2-5 > li {
        counter-increment: lst-ctn-kix_list_2-5
    }

    .lst-kix_list_2-8 > li {
        counter-increment: lst-ctn-kix_list_2-8
    }

    .lst-kix_list_1-8 > li:before {
        content: "" counter(lst-ctn-kix_list_1-8, lower-roman) ". "
    }

    .lst-kix_list_2-2 > li:before {
        content: "" counter(lst-ctn-kix_list_2-2, decimal) ". "
    }

    .lst-kix_list_2-3 > li:before {
        content: "" counter(lst-ctn-kix_list_2-3, lower-latin) ". "
    }

    .lst-kix_list_2-2 > li {
        counter-increment: lst-ctn-kix_list_2-2
    }

    ol.lst-kix_list_2-4.start {
        counter-reset: lst-ctn-kix_list_2-4 0
    }

    ol {
        margin: 0;
        padding: 0
    }

    table td, table th {
        padding: 0
    }

    .c20 {
        border-right-style: solid;
        padding: 0pt 5.8pt 0pt 5.8pt;
        border-bottom-color: #000000;
        border-top-width: 0pt;
        border-right-width: 0pt;
        border-left-color: #000000;
        vertical-align: top;
        border-right-color: #000000;
        border-left-width: 0pt;
        border-top-style: solid;
        border-left-style: solid;
        border-bottom-width: 0pt;
        width: 255.2pt;
        border-top-color: #000000;
        border-bottom-style: solid
    }

    .c7 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.0;
        orphans: 2;
        widows: 2;
        text-align: left;
        height: 11pt
    }

    .c13 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.0;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    .c5 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.3;
        text-align: center;
        height: 11pt
    }

    .c2 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.3;
        text-align: left;
        height: 11pt
    }

    .c12 {
        padding-top: 0pt;
        padding-bottom: 21.6pt;
        line-height: 1.0;
        text-align: left
    }

    .c3 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.3;
        text-align: justify
    }

    .c17 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.3;
        text-align: center
    }

    .c23 {
        margin-left: -16.9pt;
        border-spacing: 0;
        border-collapse: collapse;
        margin-right: auto
    }

    .c1 {
        color: #000000;
        text-decoration: none;
        vertical-align: baseline;
        font-style: normal
    }

    .c21 {
        background-color: #ffffff;
        max-width: 482.9pt;
        padding: 62.7pt 49.5pt 22.5pt 63pt
    }

    .c11 {
        font-size: 10pt;
        font-weight: bold
    }

    .c0 {
        font-size: 10pt;
        font-weight: 400
    }

    .c9 {
        font-weight: 400;
        font-size: 11pt;
    }

    .c10 {
        padding: 0;
        margin: 0
    }

    .c16 {
        margin-left: 36pt;
        padding-left: 0pt
    }

    .c18 {
        background-color: #ffffff;
        color: #222222
    }

    .c4 {
        margin-left: 49.5pt;
        padding-left: 0pt
    }

    .c19 {
        text-indent: -31.5pt
    }

    .c15 {
        font-style: italic
    }

    .c22 {
        font-size: 9pt
    }

    .c14 {
        height: 68pt
    }

    .c6 {
        margin-left: 31.5pt
    }

    .c8 {
        height: 11pt
    }

    .title {
        padding-top: 24pt;
        color: #000000;
        font-weight: 700;
        font-size: 36pt;
        padding-bottom: 6pt;
        line-height: 1.1500000000000001;
        page-break-after: avoid;
        text-align: left
    }

    .subtitle {
        padding-top: 18pt;
        color: #666666;
        font-size: 24pt;
        padding-bottom: 4pt;
        line-height: 1.1500000000000001;
        page-break-after: avoid;
        font-style: italic;
        text-align: left
    }

    li {
        color: #000000;
        font-size: 11pt;
    }

    p {
        margin: 0;
        color: #000000;
        font-size: 11pt;
    }

    h1 {
        padding-top: 24pt;
        color: #000000;
        font-weight: 700;
        font-size: 24pt;
        padding-bottom: 6pt;
        line-height: 1.1500000000000001;
        page-break-after: avoid;
        text-align: left
    }

    h2 {
        padding-top: 18pt;
        color: #000000;
        font-weight: 700;
        font-size: 18pt;
        padding-bottom: 4pt;
        line-height: 1.1500000000000001;
        page-break-after: avoid;
        text-align: left
    }

    h3 {
        padding-top: 14pt;
        color: #000000;
        font-weight: 700;
        font-size: 14pt;
        padding-bottom: 4pt;
        line-height: 1.1500000000000001;
        page-break-after: avoid;
        text-align: left
    }

    h4 {
        padding-top: 12pt;
        color: #000000;
        font-weight: 700;
        font-size: 12pt;
        padding-bottom: 2pt;
        line-height: 1.1500000000000001;
        page-break-after: avoid;
        text-align: left
    }

    h5 {
        padding-top: 11pt;
        color: #000000;
        font-weight: 700;
        font-size: 11pt;
        padding-bottom: 2pt;
        line-height: 1.1500000000000001;
        page-break-after: avoid;
        text-align: left
    }

    h6 {
        padding-top: 10pt;
        color: #000000;
        font-weight: 700;
        font-size: 10pt;
        padding-bottom: 2pt;
        line-height: 1.1500000000000001;
        page-break-after: avoid;
        text-align: left
    }</style>
</head>
<body class="c21">
<div><p class="c7"><span class="c1 c9"></span></p>
    <p class="c7"><span class="c1 c9"></span></p>
    <p class="c13"><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 643.80px; height: 78.53px;"><img
            alt="" src="imgs/images-template/image3.png"
            style="width: 643.80px; height: 78.53px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></p></div>
<p class="c5" id="h.gjdgxs"><span class="c1 c9"></span></p>
<p class="c17"><span class="c11">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</span>
</p>
<p class="c17"><span class="c11">&#272;&#7897;c l&#7853;p &ndash; T&#7921; do &ndash; H&#7841;nh ph&uacute;c</span></p>
<p class="c17"><span class="c11">------------o0o----------</span></p>
<p class="c5"><span class="c1 c9"></span></p>
<p class="c17"><span class="c11">HỢP ĐỒNG ĐÀO TẠO NGOẠI NGỮ</span></p>
<p class="c17"><span class="c0">S&#7889;: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; /H&#272;&#272;TNN-TPE- 2017</span>
</p>
<p class="c3"><span class="c0 c15">- C&#259;n c&#7913; B&#7897; Lu&#7853;t D&acirc;n s&#7921; s&#7889; 33/2005/QH11 &#273;&#432;&#7907;c Qu&#7889;c h&#7897;i n&#432;&#7899;c C&#7897;ng h&ograve;a x&atilde; h&#7897;i ch&#7911; ngh&#297;a Vi&#7879;t Nam th&ocirc;ng qua ng&agrave;y 14/06/2005 v&agrave; c&aacute;c v&#259;n b&#7843;n h&#432;&#7899;ng d&#7851;n thi h&agrave;nh;</span>
</p>
<p class="c3"><span class="c0 c15">- C&#259;n c&#432;&#769; va&#768;o kh&#7843; n&#259;ng v&agrave; nhu c&#7847;u th&#7921;c t&#7871; c&#7911;a hai b&ecirc;n:</span>
</p>
<p class="c3"><span class="c11">&nbsp;</span><span class="c0 c15">H&ocirc;m nay, ng&agrave;y 30 &nbsp;th&aacute;ng 10 &nbsp;n&#259;m 2017, H&#7907;p &#273;&#7891;ng n&agrave;y &#273;&#432;&#7907;c k&iacute; k&#7871;t gi&#7919;a Hai B&ecirc;n, g&#7891;m:</span>
</p>
<p class="c3"><span class="c1 c11">BÊN A &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp;{{ $a_name }}</span>
</p>
<p class="c3"><span class="c0">Ngày sinh &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $a_birthday }}</span>
</p>
<p class="c3"><span class="c0">S&#7889; CMND/H&#7897; chi&#7871;u&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;168371752 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Ng&agrave;y c&#7845;p: &nbsp; &nbsp; &nbsp;31/7/2008 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;N&#417;i c&#7845;p: &nbsp; &nbsp; &nbsp; &nbsp; H&agrave; Nam &nbsp; &nbsp; &nbsp;</span>
</p>
<p class="c3"><span class="c0">&#272;&#7883;a ch&#7881;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: B&#7857;ng Li&#7879;t, Ho&agrave;ng Mai, H&agrave; N&#7897;i</span>
</p>
<p class="c3" id="h.30j0zll"><span class="c1 c0">S&#7889; &#273;i&#7879;n tho&#7841;i&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 0947201510</span>
</p>
<p class="c3"><span class="c0">T&agrave;i kho&#7843;n s&#7889;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;t&#7841;i&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</span>
</p>
<p class="c17"><span
        class="c0 c15">&nbsp;(Sau &#273;&acirc;y g&#7885;i t&#7855;t l&agrave; &ldquo;B&ecirc;n A&rdquo;)</span></p>
<p class="c3"><span class="c11">B&Ecirc;N B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: C&Ocirc;NG TY C&#7892; PH&#7846;N GI&Aacute;O D&#7908;C TOPICA ENGLISH </span>
</p>
<p class="c3"><span class="c0">M&atilde; s&#7889; thu&#7871;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 0106291976 </span>
</p>
<p class="c3"><span class="c0">&#272;&#7883;a ch&#7881;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: T&#7847;ng 2, S&#7889; 75 Ph&#432;&#417;ng Mai, ph&#432;&#7901;ng Ph&#432;&#417;ng Mai, qu&#7853;n &#272;&#7889;ng &#272;a, H&agrave; N&#7897;i</span>
</p>
<p class="c3"><span class="c0">&#272;i&#7879;n tho&#7841;i&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 18006885 - Nh&#7845;n ph&iacute;m 1</span>
</p>
<p class="c3"><span class="c0">T&agrave;i kho&#7843;n s&#7889;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 610 932 06 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;T&#7841;i: H&#7897;i s&#7903; Ng&acirc;n h&agrave;ng Vi&#7879;t Nam Th&#7883;nh V&#432;&#7907;ng (VP Bank) </span>
</p>
<p class="c3"><span class="c0">&#272;&#7841;i di&#7879;n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &Ocirc;ng Nguy&#7877;n Danh T&uacute; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ch&#7913;c v&#7909;: Quy&#7873;n Gi&aacute;m &#273;&#7889;c Kh&#7889;i </span>
</p>
<p class="c3"><span class="c0 c15">(Theo Gi&#7845;y &#7911;y quy&#7873;n s&#7889; 19515/2016/GUQ/TPE c&#7911;a T&#7893;ng gi&aacute;m &#273;&#7889;c C&ocirc;ng ty c&#7893; ph&#7847;n gi&aacute;o d&#7909;c Topica English ng&agrave;y 26/11/2016)</span>
</p>
<p class="c17"><span class="c0 c15">(Sau &#273;&acirc;y g&#7885;i t&#7855;t l&agrave; &ldquo;B&ecirc;n B&rdquo;)</span>
</p>
<p class="c3"><span class="c0">Sau &#273;&acirc;y, B&ecirc;n A v&agrave; B&ecirc;n B g&#7885;i ri&ecirc;ng l&agrave; B&ecirc;n, g&#7885;i chung l&agrave; Hai B&ecirc;n ho&#7863;c C&aacute;c B&ecirc;n t&ugrave;y v&agrave;o ng&#7919; c&#7843;nh c&#7909; th&#7875;. </span>
</p>
<p class="c3"><span class="c0">Hai B&ecirc;n th&#7887;a thu&#7853;n k&yacute; k&#7871;t H&#7907;p &#273;&#7891;ng v&#7899;i c&aacute;c &#273;i&#7873;u kho&#7843;n sau:</span>
</p>
<p class="c3"><span class="c11">&#272;I&#7872;U 1: N&#7896;I DUNG H&#7906;P &#272;&#7890;NG</span></p>
<p class="c3"><span class="c0">B&ecirc;n A &#273;&#7891;ng &yacute; s&#7917; d&#7909;ng Kh&oacute;a &#273;&agrave;o t&#7841;o Ti&#7871;ng Anh tr&#7921;c tuy&#7871;n </span><span
        class="c0 c15">(sau &#273;&acirc;y g&#7885;i l&agrave; &ldquo;Kh&oacute;a &#273;&agrave;o t&#7841;o&rdquo;) </span><span
        class="c0">do B&ecirc;n B cung c&#7845;p, chi ti&#7871;t nh&#432; sau:</span></p>
<p class="c3"><span class="c0">1.1.T&ecirc;n Kh&oacute;a h&#7885;c&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span><span
        class="c11">Ti&#7871;ng Anh giao ti&#7871;p (NATIVE VIP 1 &ndash; 3)</span></p>
<p class="c3"><span class="c0">1.2 M&atilde; G&oacute;i h&#7885;c&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span><span
        class="c1 c11">VIP180</span></p>
<p class="c3"><span class="c0">1.3.Th&#7901;i gian &#273;&agrave;o t&#7841;o&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span><span
        class="c11">180 </span><span class="c0">(B&#7857;ng ch&#7919;: M&#7897;t tr&#259;m t&aacute;m m&#432;&#417;i) ng&agrave;y, k&#7875; t&#7915; ng&agrave;y B&ecirc;n A k&iacute;ch ho&#7841;t T&agrave;i kho&#7843;n h&#7885;c t&#7853;p tr&ecirc;n h&#7879; th&#7889;ng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (</span><span
        class="c0 c15">sau &#273;&acirc;y g&#7885;i t&#7855;t l&agrave; &ldquo;T&agrave;i kho&#7843;n&rdquo;)</span><span
        class="c0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
</p>
<p class="c3"><span class="c0">1.4.Th&#7901;i gian b&#7843;o l&#432;u t&#7889;i &#273;a&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span><span
        class="c11">60 </span><span class="c0">(B&#7857;ng ch&#7919;: S&aacute;u m&#432;&#417;i) ng&agrave;y, cho c&#7843; G&oacute;i h&#7885;c &nbsp; </span>
</p>
<p class="c3"><span class="c0">1.5 S&#7889; l&#7847;n b&#7843;o l&#432;u t&#7889;i &#273;a&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span><span
        class="c11">&nbsp;12 </span><span class="c0">(B&#7857;ng ch&#7919;: M&#432;&#7901;i hai) l&#7847;n;</span></p>
<p class="c3"><span class="c11">&#272;I&#7872;U 2: QUY&#7872;N V&Agrave; NGH&#296;A V&#7908; C&#7910;A C&Aacute;C B&Ecirc;N H&#7906;P &#272;&#7890;NG</span>
</p>
<p class="c3"><span class="c11">2.1. Quy&#7873;n v&agrave; ngh&#297;a v&#7909; c&#7911;a B&ecirc;n A:</span></p>
<p class="c3"><span class="c0">- Cam k&#7871;t h&#7885;c t&#7853;p theo &#273;&uacute;ng l&#7897; tr&igrave;nh v&agrave; h&#432;&#7899;ng d&#7851;n c&#7911;a B&ecirc;n B &#273;&#7873; ra;</span>
</p>
<p class="c3"><span class="c0">- Tu&acirc;n th&#7911; c&aacute;c quy &#273;&#7883;nh v&#7873; h&#432;&#7899;ng d&#7851;n k&#7929; thu&#7853;t, n&#7897;i quy l&#7899;p h&#7885;c v&agrave; c&aacute;c quy &#273;&#7883;nh c&#7911;a B&ecirc;n B;</span>
</p>
<p class="c3"><span class="c0">- Tu&acirc;n th&#7911; c&aacute;c quy &#273;&#7883;nh c&#7911;a B&ecirc;n B v&#7873; b&#7843;o l&#432;u v&agrave; k&iacute;ch ho&#7841;t T&agrave;i kho&#7843;n;</span>
</p>
<p class="c3"><span class="c0">- B&ecirc;n A ph&#7843;i k&iacute;ch ho&#7841;t T&agrave;i kho&#7843;n trong v&ograve;ng 01 (m&#7897;t) th&aacute;ng k&#7875; t&#7915; ng&agrave;y &#273;&#432;&#7907;c c&#7845;p m&atilde; t&agrave;i kho&#7843;n t&#7915; B&ecirc;n B. N&#7871;u sau th&#7901;i &#273;i&#7875;m n&agrave;y m&agrave; B&ecirc;n A kh&ocirc;ng ti&#7871;n h&agrave;nh k&iacute;ch ho&#7841;t T&agrave;i kho&#7843;n, T&agrave;i kho&#7843;n c&#7911;a B&ecirc;n A s&#7869; &#273;&#432;&#7907;c B&ecirc;n B t&#7921; &#273;&#7897;ng k&iacute;ch ho&#7841;t.</span>
</p>
<p class="c3"><span class="c0">- Tr&#432;&#7901;ng h&#7907;p B&ecirc;n A &#273;&#432;&#7907;c &aacute;p d&#7909;ng m&#7913;c h&#7885;c ph&iacute; &#273;&#7889;i v&#7899;i nh&oacute;m kh&aacute;ch h&agrave;ng theo quy &#273;&#7883;nh c&#7911;a B&ecirc;n B, n&#7871;u B&ecirc;n A vi ph&#7841;m m&#7897;t trong c&aacute;c &#273;i&#7873;u kho&#7843;n quy &#273;&#7883;nh t&#7841;i H&#7907;p &#273;&#7891;ng n&agrave;y th&igrave; B&ecirc;n A c&oacute; tr&aacute;ch nhi&#7879;m ho&agrave;n tr&#7843; cho B&ecirc;n B ph&#7847;n ch&ecirc;nh l&#7879;ch gi&#7919;a m&#7913;c h&#7885;c ph&iacute; d&agrave;nh cho kh&aacute;ch h&agrave;ng ri&ecirc;ng l&#7867; v&agrave; m&#7913;c h&#7885;c ph&iacute; m&agrave; B&ecirc;n A &#273;&atilde; &#273;&#432;&#7907;c &aacute;p d&#7909;ng. C&aacute;c m&#7913;c h&#7885;c ph&iacute; d&agrave;nh cho c&aacute;c nh&oacute;m kh&aacute;ch h&agrave;ng do B&ecirc;n B quy &#273;&#7883;nh. B&ecirc;n B s&#7869; th&ocirc;ng b&aacute;o cho B&ecirc;n A bi&#7871;t v&#7873; h&agrave;nh vi vi ph&#7841;m trong th&#7901;i h&#7841;n 24h (hai m&#432;&#417;i t&#432; gi&#7901;) k&#7875; t&#7915; khi x&#7843;y ra h&agrave;nh vi vi ph&#7841;m. Th&#7901;i h&#7841;n ho&agrave;n tr&#7843; ph&#7847;n ch&ecirc;nh l&#7879;ch tr&ecirc;n l&agrave; 05 (n&#259;m) ng&agrave;y k&#7875; t&#7915; ng&agrave;y B&ecirc;n B g&#7917;i th&ocirc;ng b&aacute;o b&#7857;ng th&#432; &#273;i&#7879;n t&#7917; v&agrave; v&#259;n b&#7843;n theo &#273;&#7883;a ch&#7881; B&ecirc;n A cung c&#7845;p. H&#7871;t th&#7901;i h&#7841;n n&agrave;y, n&#7871;u B&ecirc;n A kh&ocirc;ng th&#7921;c hi&#7879;n tr&aacute;ch nhi&#7879;m ho&agrave;n tr&#7843; th&igrave; B&ecirc;n B c&oacute; quy&#7873;n kh&oacute;a T&agrave;i kho&#7843;n c&#7911;a B&ecirc;n A;</span>
</p>
<p class="c3"><span class="c0">- Ph&#7889;i h&#7907;p v&#7899;i B&ecirc;n B &#273;&#7875; gi&#7843;i quy&#7871;t c&aacute;c v&#432;&#7899;ng m&#7855;c, ph&aacute;t sinh trong qu&aacute; tr&igrave;nh h&#7885;c t&#7853;p;</span>
</p>
<p class="c3"><span
        class="c0">- Thanh to&aacute;n &#273;&#7847;y &#273;&#7911; h&#7885;c ph&iacute; cho B&ecirc;n B.</span></p>
<p class="c3"><span class="c0">- Kh&ocirc;ng &#273;&#432;&#7907;c cung c&#7845;p, rao b&aacute;n, mua b&aacute;n, chuy&#7875;n nh&#432;&#7907;ng, ti&#7871;t l&#7897; th&ocirc;ng tin v&#7873; n&#7897;i dung kh&oacute;a h&#7885;c v&agrave; t&agrave;i kho&#7843;n cho b&#7845;t k&#7923; b&ecirc;n th&#7913; ba n&agrave;o kh&aacute;c, kh&ocirc;ng t&#7841;o &#273;i&#7873;u ki&#7879;n v&agrave;/ ho&#7863;c &#273;&#7875; b&ecirc;n th&#7913; ba n&agrave;o kh&aacute;c c&ugrave;ng s&#7917; d&#7909;ng ho&#7863;c s&#7917; d&#7909;ng T&agrave;i kho&#7843;n c&#7911;a B&ecirc;n A &#273;&#7875; h&#7885;c t&#7853;p m&agrave; kh&ocirc;ng c&oacute; s&#7921; &#273;&#7891;ng &yacute; tr&#432;&#7899;c b&#7857;ng v&#259;n b&#7843;n c&#7911;a B&ecirc;n B. </span>
</p>
<p class="c3"><span class="c0">Trong tr&#432;&#7901;ng h&#7907;p B&ecirc;n B ph&aacute;t hi&#7879;n ra c&oacute; b&#7845;t k&#7923; vi ph&#7841;m n&ecirc;u tr&ecirc;n, B&ecirc;n B c&oacute; quy&#7873;n kh&oacute;a T&agrave;i kho&#7843;n c&#7911;a B&ecirc;n A ngay l&#7853;p t&#7913;c m&agrave; kh&ocirc;ng c&#7847;n th&ocirc;ng b&aacute;o tr&#432;&#7899;c cho B&ecirc;n A, &#273;&#7891;ng th&#7901;i, B&ecirc;n A ph&#7843;i ch&#7883;u ph&#7841;t vi ph&#7841;m theo quy &#273;&#7883;nh t&#7841;i &#272;i&#7873;u 9 H&#7907;p &#273;&#7891;ng n&agrave;y.</span>
</p>
<p class="c3"><span class="c11">2.2. Tr&aacute;ch nhi&#7879;m c&#7911;a B&ecirc;n B:</span></p>
<p class="c3"><span class="c0">- Gi&#7843;ng d&#7841;y &#273;&uacute;ng theo ch&#432;&#417;ng tr&igrave;nh v&agrave; ti&#7871;n &#273;&#7897; h&#7885;c t&#7853;p &#273;&atilde; th&#7889;ng nh&#7845;t gi&#7919;a Hai B&ecirc;n;</span>
</p>
<p class="c3"><span class="c0">- &#272;&aacute;nh gi&aacute; k&#7871;t qu&#7843; v&agrave; c&#7845;p ch&#7913;ng nh&#7853;n cho c&aacute;c h&#7885;c vi&ecirc;n sau khi ho&agrave;n th&agrave;nh Kh&oacute;a &#273;&agrave;o t&#7841;o;</span>
</p>
<p class="c3"><span class="c0">- Cung c&#7845;p T&agrave;i kho&#7843;n v&agrave; m&#7853;t kh&#7849;u h&#7885;c vi&ecirc;n cho B&ecirc;n A ch&#7853;m nh&#7845;t trong v&ograve;ng 03 (ba) ng&agrave;y k&#7875; t&#7915; ng&agrave;y nh&#7853;n thanh to&aacute;n h&#7885;c ph&iacute; theo quy &#273;&#7883;nh t&#7841;i &#272;i&#7873;u 3 c&#7911;a H&#7907;p &#273;&#7891;ng n&agrave;y.</span>
</p>
<p class="c3"><span class="c0">- Trong 15 (m&#432;&#7901;i l&#259;m) ng&agrave;y l&agrave;m vi&#7879;c k&#7875; t&#7915; ng&agrave;y B&ecirc;n B nh&#7853;n &#273;&#432;&#7907;c 100% h&#7885;c ph&iacute; t&#7915; B&ecirc;n A, B&ecirc;n B c&oacute; tr&aacute;ch nhi&#7879;m cung c&#7845;p h&oacute;a &#273;&#417;n t&agrave;i ch&iacute;nh 01 (m&#7897;t) l&#7847;n cho B&ecirc;n A. </span>
</p>
<p class="c3"><span class="c0">- Trong tr&#432;&#7901;ng h&#7907;p B&ecirc;n A h&#7885;c t&#7853;p theo &#273;&uacute;ng quy &#273;&#7883;nh v&agrave; h&#432;&#7899;ng d&#7851;n c&#7911;a B&ecirc;n B m&agrave; kh&ocirc;ng &#273;&#7841;t &#273;&#432;&#7907;c &#273;i&#7875;m s&#7889; cam k&#7871;t th&igrave; B&ecirc;n B s&#7869; h&#7895; tr&#7907; mi&#7877;n ph&iacute; cho B&ecirc;n A m&#7897;t kh&oacute;a h&#7885;c &#273;&#7875; B&ecirc;n A &#273;&#7841;t &#273;&#432;&#7907;c &#273;i&#7875;m s&#7889; nh&#432; cam k&#7871;t theo l&#7897; tr&igrave;nh h&#7885;c t&#7853;p Hai B&ecirc;n &#273;&atilde; x&acirc;y d&#7921;ng. Trong tr&#432;&#7901;ng h&#7907;p n&agrave;y, Hai B&ecirc;n s&#7869; th&#7887;a thu&#7853;n c&#7909; th&#7875; v&agrave; l&#7853;p th&agrave;nh Ph&#7909; l&#7909;c H&#7907;p &#273;&#7891;ng. </span>
</p>
<p class="c3"><span class="c0">- &#272;&#432;&#7907;c nh&#7853;n ph&iacute; b&#7891;i th&#432;&#7901;ng vi ph&#7841;m H&#7907;p &#273;&#7891;ng t&#7915; B&ecirc;n A trong tr&#432;&#7901;ng h&#7907;p B&ecirc;n A cung c&#7845;p, rao b&aacute;n, mua b&aacute;n, chuy&#7875;n nh&#432;&#7907;ng quy&#7873;n s&#7917; d&#7909;ng T&agrave;i kho&#7843;n cho b&#7845;t c&#7913; b&ecirc;n th&#7913; ba n&agrave;o kh&aacute;c, b&#7845;t k&#7875; b&ecirc;n th&#7913; ba &#273;&oacute; c&oacute; s&#7917; d&#7909;ng T&agrave;i kho&#7843;n &#273;&#7875; h&#7885;c t&#7853;p hay kh&ocirc;ng. M&#7913;c b&#7891;i th&#432;&#7901;ng s&#7869; d&#7921;a tr&ecirc;n m&#7913;c &#273;&#7897; thi&#7879;t h&#7841;i th&#7921;c t&#7871; c&#7911;a B&ecirc;n B m&agrave; Hai B&ecirc;n th&#7889;ng nh&#7845;t x&aacute;c &#273;&#7883;nh.</span>
</p>
<p class="c3"><span class="c0">- Trong tr&#432;&#7901;ng h&#7907;p B&ecirc;n A vi ph&#7841;m c&aacute;c &#273;i&#7873;u kho&#7843;n c&#7911;a H&#7907;p &#273;&#7891;ng n&agrave;y, B&ecirc;n B kh&ocirc;ng c&oacute; tr&aacute;ch nhi&#7879;m ph&#7843;i &#273;&#7843;m b&#7843;o &#273;i&#7875;m s&#7889; cam k&#7871;t nh&#432; ban &#273;&#7847;u cho B&ecirc;n A ho&#7863;c b&#7845;t c&#7913; b&ecirc;n n&agrave;o kh&aacute;c tham gia s&#7917; d&#7909;ng T&agrave;i kho&#7843;n c&#7911;a B&ecirc;n A.</span>
</p>
<p class="c3"><span class="c11">&#272;I&#7872;U 3: CHUY&#7874;N NH&#431;&#7906;NG T&Agrave;I KHO&#7842;N</span></p>
<p class="c3"><span class="c0">3.1 B&ecirc;n A kh&ocirc;ng &#273;&#432;&#7907;c ph&eacute;p chuy&#7875;n nh&#432;&#7907;ng T&agrave;i kho&#7843;n cho b&#7845;t k&#7923; B&ecirc;n th&#7913; ba n&agrave;o kh&aacute;c ngo&agrave;i H&#7907;p &#273;&#7891;ng n&agrave;y m&agrave; ch&#432;a c&oacute; s&#7921; &#273;&#7891;ng &yacute; tr&#432;&#7899;c b&#7857;ng v&#259;n b&#7843;n c&#7911;a B&ecirc;n B. </span>
</p>
<p class="c3"><span class="c0">3.2 &#272;i&#7873;u ki&#7879;n chuy&#7875;n nh&#432;&#7907;ng:</span></p>
<p class="c3"><span class="c0">- T&agrave;i kho&#7843;n c&#7911;a B&ecirc;n A c&ograve;n &iacute;t nh&#7845;t 50% t&#7893;ng s&#7889; th&#7901;i gian &#273;&agrave;o t&#7841;o c&#7911;a Kh&oacute;a &#273;&agrave;o t&#7841;o;</span>
</p>
<p class="c3"><span class="c0">- T&agrave;i kho&#7843;n ch&#432;a bao gi&#7901; chuy&#7875;n nh&#432;&#7907;ng;</span>
</p>
<p class="c3"><span class="c0">- B&ecirc;n A &#273;&oacute;ng Ph&iacute; chuy&#7875;n nh&#432;&#7907;ng l&agrave; 500.000 VN&#272;/t&agrave;i kho&#7843;n.</span>
</p>
<p class="c3"><span class="c0">3.3 Trong tr&#432;&#7901;ng h&#7907;p B&ecirc;n A chuy&#7875;n nh&#432;&#7907;ng T&agrave;i kho&#7843;n m&agrave; kh&ocirc;ng c&oacute; s&#7921; &#273;&#7891;ng &yacute; c&#7911;a B&ecirc;n B, B&ecirc;n B c&oacute; quy&#7873;n kh&oacute;a T&agrave;i kho&#7843;n c&#7911;a B&ecirc;n A ngay l&#7853;p t&#7913;c &#273;&#7891;ng th&#7901;i ph&#7841;t B&ecirc;n A ph&iacute; vi ph&#7841;m chuy&#7875;n nh&#432;&#7907;ng t&#432;&#417;ng &#273;&#432;&#417;ng v&#7899;i: 8% m&#7913;c h&#7885;c ph&iacute; n&ecirc;u t&#7841;i &#272;i&#7875;m b &#272;i&#7873;u 4.1 H&#7907;p &#273;&#7891;ng n&agrave;y ho&#7863;c 500,000 VN&#272; (N&#259;m tr&#259;m ng&agrave;n &#273;&#7891;ng ch&#7861;n) t&ugrave;y theo gi&aacute; tr&#7883; n&agrave;o cao h&#417;n. Gi&aacute; tr&#7883; n&agrave;y &#273;&atilde; bao g&#7891;m m&#7885;i lo&#7841;i thu&#7871;, ph&iacute; (n&#7871;u c&oacute;). </span>
</p>
<p class="c3"><span class="c0">3.6 C&aacute;c quy &#273;&#7883;nh kh&aacute;c v&#7873; chuy&#7875;n nh&#432;&#7907;ng t&agrave;i kho&#7843;n h&#7885;c t&#7853;p m&agrave; B&ecirc;n A ban h&agrave;nh.</span>
</p>
<p class="c3"><span class="c11">&#272;I&#7872;U 4: H&#7884;C PH&Iacute; V&Agrave; &#272;I&#7872;U KHO&#7842;N THANH TO&Aacute;N</span>
</p>
<p class="c3"><span class="c0">4.1 H&#7885;c ph&iacute; l&agrave; kho&#7843;n ti&#7873;n B&ecirc;n A ph&#7843;i thanh to&aacute;n cho B&ecirc;n B &#273;&#7875; &#273;&#432;&#7907;c s&#7917; d&#7909;ng Kh&oacute;a &#273;&agrave;o t&#7841;o n&ecirc;u t&#7841;i &#272;i&#7873;u 1. M&#7913;c h&#7885;c ph&iacute; Kh&oacute;a &#273;&agrave;o t&#7841;o nh&#432; sau:</span>
</p>
<p class="c3"><span class="c0">a. Tr&#432;&#7901;ng h&#7907;p B&ecirc;n A ho&agrave;n thi&#7879;n n&#7897;p h&#7885;c ph&iacute; tr&#432;&#7899;c v&agrave; trong ng&agrave;y </span><span
        class="c11">&hellip;&hellip;&hellip;&hellip;&hellip;:</span><span class="c0">&nbsp;B&ecirc;n A &#273;&#432;&#7907;c &aacute;p d&#7909;ng m&#7913;c h&#7885;c ph&iacute; &#432;u &#273;&atilde;i l&agrave;: </span><span
        class="c11">&hellip;&hellip;&hellip;&hellip;&hellip;.. &#273;&#7891;ng</span><span class="c0">.</span></p>
<p class="c3"><span class="c0">(B&#7857;ng ch&#7919;: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; &#273;&#7891;ng)</span>
</p>
<p class="c3"><span class="c0">b Tr&#432;&#7901;ng h&#7907;p B&ecirc;n A ho&agrave;n thi&#7879;n n&#7897;p h&#7885;c ph&iacute; sau ng&agrave;y </span><span
        class="c11">&hellip;&hellip;&hellip;&hellip;&hellip;</span><span class="c0">&nbsp;: B&ecirc;n A &#273;&#432;&#7907;c &aacute;p d&#7909;ng m&#7913;c h&#7885;c ph&iacute; l&agrave;: </span><span
        class="c11">&hellip;&hellip;&hellip;&hellip;&hellip;.. &#273;&#7891;ng</span><span class="c0">&nbsp;</span></p>
<p class="c3"><span class="c0">(B&#7857;ng ch&#7919;:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&#273;&#7891;ng)</span>
</p>
<p class="c3"><span class="c0">+ &#431;u &#273;&atilde;i : &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span>
</p>
<p class="c3"><span class="c0">4.2 H&igrave;nh th&#7913;c thanh to&aacute;n: L&#7921;a ch&#7885;n ph&#432;&#417;ng th&#7913;c thanh to&aacute;n b&#7857;ng c&aacute;ch &#273;&aacute;nh d&#7845;u &ldquo;x&rdquo; v&agrave;o &ocirc; vu&ocirc;ng b&ecirc;n tr&aacute;i m&#7895;i d&ograve;ng.</span>
</p>
<p class="c3 c6 c19"><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chuy&#7875;n kho&#7843;n: Theo th&ocirc;ng tin t&agrave;i kho&#7843;n c&#7911;a B&ecirc;n B</span><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
        alt="" src="imgs/images-template/image1.png"
        style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title="" /></span></p>
<p class="c3"><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; N&#7897;p ti&#7873;n m&#7863;t t&#7841;i Tr&#7909; s&#7903;/ chi nh&aacute;nh/V&#259;n ph&ograve;ng &#273;&#7841;i di&#7879;n c&#7911;a B&ecirc;n B</span><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
        alt="" src="imgs/images-template/image1.png"
        style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title="" /></span></p>
<p class="c3"><span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Thu ti&#7873;n tr&#7921;c ti&#7871;p t&#7841;i &#273;&#7883;a ch&#7881; c&#7911;a B&ecirc;n A (COD).</span><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 13.00px; height: 10.33px;"><img
        alt="" src="imgs/images-template/image4.png"
        style="width: 13.00px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title="" /></span></p>
<p class="c3"><span class="c0">4.3 &#272;&#7889;i v&#7899;i h&igrave;nh th&#7913;c chuy&#7875;n kho&#7843;n, B&ecirc;n A l&#7921;a ch&#7885;n m&#7897;t trong c&aacute;c t&agrave;i kho&#7843;n c&#7911;a B&ecirc;n B &#273;&#7875; thanh to&aacute;n (b&#7857;ng c&aacute;ch &#273;&aacute;nh d&#7845;u &ldquo;x&rdquo; v&agrave;o &ocirc; vu&ocirc;ng b&ecirc;n tr&aacute;i s&#7889; t&agrave;i kho&#7843;n) theo th&ocirc;ng tin sau:</span>
</p>
<p class="c17"><span class="c11">CH&#7910; T&Agrave;I KHO&#7842;N: C&Ocirc;NG TY C&#7892; PH&#7846;N GI&Aacute;O D&#7908;C TOPICA ENGLISH</span>
</p>
<ol class="c10 lst-kix_list_1-0 start" start="1">
    <li class="c3 c4"><span class="c1 c0">S&#7889; t&agrave;i kho&#7843;n: 0011 0041 64283 &nbsp; </span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
            alt="" src="imgs/images-template/image1.png"
            style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></li>
</ol>
<p class="c3 c6"><span class="c0">Ng&acirc;n h&agrave;ng: Ngo&#7841;i th&#432;&#417;ng Vi&#7879;t nam (Vietcombank) - chi nh&aacute;nh S&#7903; Giao D&#7883;ch Vietcombank.</span>
</p>
<ol class="c10 lst-kix_list_1-0" start="2">
    <li class="c3 c4"><span class="c1 c0">S&#7889; t&agrave;i kho&#7843;n: 1221 0000 6875 53 </span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
            alt="" src="imgs/images-template/image1.png"
            style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></li>
</ol>
<p class="c3 c6"><span class="c0">Ng&acirc;n h&agrave;ng: &#272;&#7847;u t&#432; v&agrave; ph&aacute;t tri&#7875;n Vi&#7879;t Nam (BIDV) - Chi nh&aacute;nh H&agrave; Th&agrave;nh </span>
</p>
<ol class="c10 lst-kix_list_1-0" start="3">
    <li class="c3 c4"><span class="c1 c0">S&#7889; t&agrave;i kho&#7843;n: 6109 3206 </span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 10.67px; height: 9.33px;"><img
            alt="" src="imgs/images-template/image2.png"
            style="width: 10.67px; height: 9.33px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
            alt="" src="imgs/images-template/image1.png"
            style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></li>
</ol>
<p class="c3 c6"><span class="c0">Ng&acirc;n h&agrave;ng: Vi&#7879;t Nam Th&#7883;nh V&#432;&#7907;ng (VPBank) - Chi nh&aacute;nh S&#7903; giao d&#7883;ch.</span>
</p>
<ol class="c10 lst-kix_list_1-0" start="4">
    <li class="c3 c4"><span class="c0 c1">S&#7889; t&agrave;i kho&#7843;n: &nbsp;1912 8170 6360 12 </span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 10.67px; height: 9.33px;"><img
            alt="" src="imgs/images-template/image2.png"
            style="width: 10.67px; height: 9.33px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
            alt="" src="imgs/images-template/image1.png"
            style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></li>
</ol>
<p class="c3 c6"><span class="c0">Ng&acirc;n h&agrave;ng: K&#7929; th&#432;&#417;ng Vi&#7879;t Nam (Techcombank) - Chi nh&aacute;nh Th&#259;ng Long.</span>
</p>
<ol class="c10 lst-kix_list_1-0" start="5">
    <li class="c3 c4"><span class="c0">S&#7889; t&agrave;i kho&#7843;n: &nbsp;</span><span class="c0 c18">115 000 130 666</span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 10.67px; height: 9.33px;"><img
            alt="" src="imgs/images-template/image2.png"
            style="width: 10.67px; height: 9.33px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
            alt="" src="imgs/images-template/image1.png"
            style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></li>
</ol>
<p class="c3 c6"><span class="c0">Ng&acirc;n h&agrave;ng: C&ocirc;ng th&#432;&#417;ng Vi&#7879;t Nam (Vietinbank) - Chi nh&aacute;nh Ho&agrave;ng Mai.</span>
</p>
<ol class="c10 lst-kix_list_1-0" start="6">
    <li class="c3 c4"><span class="c1 c0">S&#7889; t&agrave;i kho&#7843;n: 1500 2010 89256 &nbsp;</span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 10.67px; height: 9.33px;"><img
            alt="" src="imgs/images-template/image2.png"
            style="width: 10.67px; height: 9.33px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
            alt="" src="imgs/images-template/image1.png"
            style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></li>
</ol>
<p class="c3 c6"><span class="c0">Ng&acirc;n h&agrave;ng: N&ocirc;ng nghi&#7879;p v&agrave; ph&aacute;t tri&#7875;n n&ocirc;ng th&ocirc;n Vi&#7879;t nam (Agribank) - chi nh&aacute;nh H&agrave; N&#7897;i.</span>
</p>
<ol class="c10 lst-kix_list_1-0" start="7">
    <li class="c3 c4"><span class="c1 c0">S&#7889; t&agrave;i kho&#7843;n: 203 383 399 &nbsp;</span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 10.67px; height: 9.33px;"><img
            alt="" src="imgs/images-template/image2.png"
            style="width: 10.67px; height: 9.33px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
            alt="" src="imgs/images-template/image1.png"
            style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></li>
</ol>
<p class="c3 c6"><span class="c0">Ng&acirc;n h&agrave;ng: Th&#432;&#417;ng m&#7841;i c&#7893; ph&#7847;n &Aacute; Ch&acirc;u (ACB) - chi nh&aacute;nh H&agrave; N&#7897;i</span>
</p>
<ol class="c10 lst-kix_list_1-0" start="8">
    <li class="c3 c4"><span class="c1 c0">S&#7889; t&agrave;i kho&#7843;n: 0200 3736 3333 &nbsp;</span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 10.67px; height: 9.33px;"><img
            alt="" src="imgs/images-template/image2.png"
            style="width: 10.67px; height: 9.33px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span><span
            style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 11.67px; height: 10.33px;"><img
            alt="" src="imgs/images-template/image1.png"
            style="width: 11.67px; height: 10.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
            title="" /></span></li>
</ol>
<p class="c3 c6"><span class="c0">Ng&acirc;n h&agrave;ng: Th&#432;&#417;ng m&#7841;i c&#7893; ph&#7847;n S&agrave;i G&ograve;n Th&#432;&#417;ng T&iacute;n (Sacombank) - chi nh&aacute;nh 8 th&aacute;ng 3 H&agrave; N&#7897;i</span>
</p>
<p class="c3"><span class="c11">&#272;I&#7872;U 5: R&Uacute;T H&#7884;C PH&Iacute;</span></p>
<p class="c3"><span class="c0">5.1 B&ecirc;n A ch&#7881; &#273;&#432;&#7907;c ph&eacute;p r&uacute;t h&#7885;c ph&iacute; khi thu&#7897;c trong c&aacute;c tr&#432;&#7901;ng h&#7907;p &#273;&#7863;c bi&#7879;t &#273;&#432;&#7907;c r&uacute;t h&#7885;c ph&iacute; do B&ecirc;n B quy &#273;&#7883;nh.</span>
</p>
<p class="c3"><span class="c0">5.2 N&#7871;u thu&#7897;c m&#7897;t trong c&aacute;c tr&#432;&#7901;ng h&#7907;p &#273;&#7863;c bi&#7879;t do B&ecirc;n B quy &#273;&#7883;nh, B&ecirc;n A c&oacute; quy&#7873;n r&uacute;t h&#7885;c ph&iacute; theo quy tr&igrave;nh m&agrave; B&ecirc;n B ban h&agrave;nh trong v&ograve;ng 33 ng&agrave;y t&iacute;nh t&#7915; ng&agrave;y n&#7897;p &#273;&#7911; 100% h&#7885;c ph&iacute;. Th&#7911; t&#7909;c c&#7909; th&#7875; v&#7873; vi&#7879;c r&uacute;t h&#7885;c ph&iacute; s&#7869; &#273;&#432;&#7907;c nh&acirc;n vi&ecirc;n c&#7911;a B&ecirc;n B t&#432; v&#7845;n h&#7895; tr&#7907; B&ecirc;n A th&#7921;c hi&#7879;n.</span>
</p>
<p class="c3"><span
        class="c11">&#272;I&#7872;U 6: TH&#7900;I GIAN TH&#7920;C HI&#7878;N H&#7906;P &#272;&#7890;NG</span></p>
<p class="c3"><span class="c0">6.1 H&#7907;p &#273;&#7891;ng n&agrave;y c&oacute; hi&#7879;u l&#7921;c k&#7875; t&#7915; ng&agrave;y k&yacute; v&agrave; t&#7921; &#273;&#7897;ng thanh l&yacute; khi x&#7843;y ra m&#7897;t trong c&aacute;c tr&#432;&#7901;ng h&#7907;p sau:</span>
</p>
<ol class="c10 lst-kix_list_2-0 start" start="1">
    <li class="c3 c16"><span class="c1 c0">Hai B&ecirc;n k&#7871;t th&uacute;c Th&#7901;i gian &#273;&agrave;o t&#7841;o.</span>
    </li>
    <li class="c3 c16"><span class="c1 c0">Hai B&ecirc;n c&ugrave;ng th&#7887;a thu&#7853;n ch&#7845;m d&#7913;t H&#7907;p &#273;&#7891;ng tr&#432;&#7899;c th&#7901;i h&#7841;n.</span>
    </li>
    <li class="c3 c16"><span class="c1 c0">M&#7897;t trong hai B&ecirc;n vi ph&#7841;m c&aacute;c &#273;i&#7873;u kho&#7843;n trong H&#7907;p &#273;&#7891;ng n&agrave;y. </span>
    </li>
</ol>
<p class="c3"><span class="c0">6.2 Vi&#7879;c H&#7907;p &#273;&#7891;ng n&agrave;y k&#7871;t th&uacute;c kh&ocirc;ng &#7843;nh h&#432;&#7903;ng &#273;&#7871;n c&aacute;c quy&#7873;n v&agrave; ngh&#297;a v&#7909; ch&#432;a ho&agrave;n th&agrave;nh c&#7911;a c&aacute;c B&ecirc;n trong H&#7907;p &#273;&#7891;ng. C&aacute;c B&ecirc;n c&oacute; ngh&#297;a v&#7909; ho&agrave;n th&agrave;nh c&aacute;c tr&aacute;ch nhi&#7879;m ch&#432;a th&#7921;c hi&#7879;n xong theo H&#7907;p &#273;&#7891;ng n&agrave;y v&#7899;i B&ecirc;n c&ograve;n l&#7841;i trong v&ograve;ng l&acirc;u nh&#7845;t l&agrave; 03 (ba) th&aacute;ng k&#7875; t&#7915; th&#7901;i &#273;i&#7875;m H&#7907;p &#273;&#7891;ng n&agrave;y ch&#7845;m d&#7913;t.</span>
</p>
<p class="c3"><span class="c11">&#272;I&#7872;U 7: B&#7844;T KH&#7842; KH&Aacute;NG </span></p>
<p class="c3"><span class="c0">7.1 Theo c&aacute;c m&#7909;c &#273;&iacute;ch c&#7911;a &#273;i&#7873;u kho&#7843;n n&agrave;y, &ldquo;B&#7845;t kh&#7843; kh&aacute;ng&rdquo; c&oacute; ngh&#297;a l&agrave; b&#7845;t k&#7923; m&#7897;t t&igrave;nh hu&#7889;ng n&agrave;o (xu&#7845;t ph&aacute;t t&#7915; c&aacute;c nguy&ecirc;n nh&acirc;n t&#7921; nhi&ecirc;n, t&aacute;c &#273;&#7897;ng c&#7911;a con ng&#432;&#7901;i hay l&yacute; do kh&aacute;c) n&#7857;m ngo&agrave;i ph&#7841;m vi ki&#7875;m so&aacute;t c&#7911;a c&aacute;c B&ecirc;n trong H&#7907;p &#273;&#7891;ng n&agrave;y bao g&#7891;m nh&#432;ng kh&ocirc;ng gi&#7899;i h&#7841;n, b&#7903;i: thi&ecirc;n tai, &#273;&#7883;ch v&#7853;n, ho&#7843; ho&#7841;n, ch&aacute;y n&#7893;, l&#361; l&#7909;t, gi&ocirc;ng b&atilde;o, chi&#7871;n tranh, b&#7841;o lo&#7841;n, ph&aacute; ho&#7841;i, c&#7845;m v&#7853;n, h&agrave;nh &#273;&#7897;ng c&#7911;a Ch&iacute;nh ph&#7911; hay s&#7921; ph&#7909;c t&ugrave;ng c&#7911;a c&aacute;c B&ecirc;n &#273;&#7889;i v&#7899;i c&aacute;c y&ecirc;u c&#7847;u, m&#7879;nh l&#7879;nh hay s&#7921; &#273;i&#7873;u khi&#7875;n c&#7911;a c&aacute;c c&#417; quan, t&#7893; ch&#7913;c, c&aacute; nh&acirc;n c&oacute; th&#7849;m quy&#7873;n theo quy &#273;&#7883;nh c&#7911;a ph&aacute;p lu&#7853;t.</span>
</p>
<p class="c3"><span class="c0">7.2 Khi c&oacute; b&#7845;t k&#7923; tr&#432;&#7901;ng h&#7907;p B&#7845;t kh&#7843; kh&aacute;ng n&agrave;o, g&acirc;y ra s&#7921; ch&#7853;m tr&#7877;, t&aacute;c &#273;&#7897;ng v&agrave;o ho&#7863;c g&acirc;y ra s&#7921; ng&#7915;ng tr&#7879; vi&#7879;c th&#7921;c hi&#7879;n ngh&#297;a v&#7909; H&#7907;p &#273;&#7891;ng c&#7911;a m&#7897;t b&ecirc;n n&agrave;o &#273;&oacute;, tr&ecirc;n c&#417; s&#7903; th&ocirc;ng b&aacute;o v&#7873; c&aacute;c tr&#432;&#7901;ng h&#7907;p B&#7845;t kh&#7843; kh&aacute;ng, tr&aacute;ch nhi&#7879;m th&#7921;c hi&#7879;n c&aacute;c ngh&#297;a v&#7909; ti&#7871;p theo trong H&#7907;p &#273;&#7891;ng c&#7911;a B&ecirc;n li&ecirc;n quan s&#7869; &#273;&#432;&#7907;c t&#7841;m ho&atilde;n ho&#7863;c gi&#7899;i h&#7841;n l&#7841;i (theo ho&agrave;n c&#7843;nh cho ph&eacute;p th&#7921;c hi&#7879;n) cho &#273;&#7871;n khi m&#7895;i tr&#432;&#7901;ng h&#7907;p B&#7845;t kh&#7843; kh&aacute;ng kh&ocirc;ng c&ograve;n n&#7919;a.</span>
</p>
<p class="c3"><span class="c0">7.3 Th&#7901;i h&#7841;n th&ocirc;ng b&aacute;o b&#7857;ng v&#259;n b&#7843;n c&#7911;a B&ecirc;n b&#7883; &#7843;nh h&#432;&#7903;ng cho B&ecirc;n c&ograve;n l&#7841;i v&#7873; t&iacute;nh ch&#7845;t v&agrave; m&#7913;c &#273;&#7897; c&#7911;a s&#7921; ki&#7879;n B&#7845;t kh&#7843; kh&aacute;ng l&agrave; 05 (n&#259;m) ng&agrave;y k&#7875; t&#7915; ng&agrave;y x&#7843;y ra s&#7921; ki&#7879;n b&#7845;t kh&#7843; kh&aacute;ng.</span>
</p>
<p class="c3"><span class="c0">7.4 B&ecirc;n kh&ocirc;ng b&#7883; &#7843;nh h&#432;&#7903;ng c&oacute; quy&#7873;n &#273;&#417;n ph&#432;&#417;ng ch&#7845;m d&#7913;t h&#7907;p &#273;&#7891;ng v&agrave; g&#7917;i th&ocirc;ng b&aacute;o b&#7857;ng v&#259;n b&#7843;n ch&iacute;nh th&#7913;c cho B&ecirc;n b&#7883; &#7843;nh h&#432;&#7903;ng n&#7871;u vi&#7879;c th&#7921;c hi&#7879;n b&#7845;t k&#7923; m&#7897;t ngh&#297;a v&#7909; n&agrave;o trong h&#7907;p &#273;&#7891;ng c&#7911;a B&ecirc;n b&#7883; &#7843;nh h&#432;&#7903;ng ch&#7853;m qu&aacute; 180 ng&agrave;y (k&#7875; t&#7915; ng&agrave;y th&ocirc;ng b&aacute;o n&ecirc;u tr&ecirc;n).</span>
</p>
<p class="c3"><span class="c11">&#272;I&#7872;U 8. B&#7842;O M&#7852;T TH&Ocirc;NG TIN V&Agrave; QUY&#7872;N S&#7902; H&#7918;U TR&Iacute; TU&#7878;</span>
</p>
<p class="c3"><span class="c11">8.1 B&#7843;o m&#7853;t th&ocirc;ng tin</span></p>
<p class="c3"><span class="c0">- M&#7895;i B&ecirc;n cam k&#7871;t b&#7843;o m&#7853;t v&agrave; kh&ocirc;ng cung c&#7845;p cho b&#7845;t k&#7923; B&ecirc;n th&#7913; ba n&agrave;o kh&aacute;c: v&#7873; n&#7897;i dung c&#7911;a H&#7907;p &#273;&#7891;ng n&agrave;y, c&aacute;c n&#7897;i dung li&ecirc;n quan &#273;&#7871;n giao d&#7883;ch gi&#7919;a Hai B&ecirc;n do m&#7897;t trong Hai B&ecirc;n cung c&#7845;p v&agrave;/ho&#7863;c do Hai B&ecirc;n ph&#7889;i h&#7907;p th&#7921;c hi&#7879;n, n&#7897;i dung c&#7911;a Kh&oacute;a h&#7885;c B&ecirc;n B cung c&#7845;p cho B&ecirc;n A ho&#7863;c b&ecirc;n th&#7913; ba &#273;&#432;&#7907;c B&ecirc;n A chuy&#7875;n nh&#432;&#7907;ng T&agrave;i kho&#7843;n&hellip; tr&#7915; tr&#432;&#7901;ng h&#7907;p &#273;&#432;&#7907;c B&ecirc;n c&ograve;n l&#7841;i ch&#7845;p thu&#7853;n b&#7857;ng v&#259;n b&#7843;n ho&#7863;c bu&#7897;c ph&#7843;i ti&#7871;t l&#7897; theo y&ecirc;u c&#7847;u c&#7911;a C&#417; quan nh&agrave; n&#432;&#7899;c c&oacute; th&#7849;m quy&#7873;n. Trong tr&#432;&#7901;ng h&#7907;p &#273;&oacute;, B&ecirc;n &#273;&#7873; ngh&#7883; ti&#7871;t l&#7897; th&ocirc;ng tin ph&#7843;i c&oacute; v&#259;n b&#7843;n th&ocirc;ng b&aacute;o cho B&ecirc;n c&ograve;n l&#7841;i trong v&ograve;ng 48h (b&#7889;n m&#432;&#417;i t&aacute;m gi&#7901;) k&#7875; t&#7915; th&#7901;i &#273;i&#7875;m nh&#7853;n &#273;&#432;&#7907;c y&ecirc;u c&#7847;u cung c&#7845;p th&ocirc;ng tin k&egrave;m theo v&#259;n b&#7843;n y&ecirc;u c&#7847;u cung c&#7845;p th&ocirc;ng tin c&#7911;a C&#417; quan nh&agrave; n&#432;&#7899;c c&oacute; th&#7849;m quy&#7873;n.</span>
</p>
<p class="c3"><span class="c0">- B&ecirc;n A cam k&#7871;t (i) Kh&ocirc;ng l&#432;u tr&#7919;, chia s&#7867; m&#7897;t ph&#7847;n v&agrave;/ho&#7863;c to&agrave;n b&#7897; c&aacute;c t&agrave;i li&#7879;u c&oacute; li&ecirc;n quan &#273;&#7871;n kh&oacute;a h&#7885;c cho b&#7845;t k&igrave; B&ecirc;n th&#7913; ba n&agrave;o kh&aacute;c; v&agrave; (ii) Kh&ocirc;ng s&#7917; d&#7909;ng m&#7897;t ph&#7847;n v&agrave;/ho&#7863;c to&agrave;n b&#7897; t&agrave;i li&#7879;u c&oacute; li&ecirc;n quan &#273;&#7871;n kh&oacute;a h&#7885;c v&agrave;o b&#7845;t k&igrave; ho&#7841;t &#273;&#7897;ng n&agrave;o m&agrave; ch&#432;a &#273;&#432;&#7907;c s&#7921; cho ph&eacute;p tr&#432;&#7899;c b&#7857;ng v&#259;n b&#7843;n c&#7911;a B&ecirc;n B ngo&#7841;i tr&#7915; vi&#7879;c s&#7917; d&#7909;ng c&aacute;c t&agrave;i li&#7879;u n&agrave;y v&agrave;o m&#7909;c &#273;&iacute;ch h&#7885;c t&#7853;p c&#7911;a B&ecirc;n A theo quy &#273;&#7883;nh t&#7841;i H&#7907;p &#273;&#7891;ng n&agrave;y.</span>
</p>
<p class="c3"><span class="c11">8.2 Quy&#7873;n S&#7903; h&#7919;u tr&iacute; tu&#7879;</span></p>
<p class="c3"><span class="c0">B&ecirc;n B l&agrave; t&aacute;c gi&#7843; v&agrave; Ch&#7911; s&#7903; h&#7919;u duy nh&#7845;t &#273;&#7889;i v&#7899;i to&agrave;n b&#7897; ph&#7847;n m&#7873;m h&#7885;c t&#7853;p v&agrave; c&aacute;c t&agrave;i li&#7879;u c&#7911;a Kh&oacute;a h&#7885;c. M&#7885;i h&agrave;nh vi li&ecirc;n quan &#273;&#7871;n ph&#7847;n m&#7873;m h&#7885;c t&#7853;p v&agrave; t&agrave;i li&#7879;u c&#7911;a Kh&oacute;a h&#7885;c bao g&#7891;m nh&#432;ng kh&ocirc;ng gi&#7899;i h&#7841;n: s&#7917; d&#7909;ng, rao b&aacute;n, ph&acirc;n ph&#7889;i, ch&#7881;nh s&#7917;a, khai th&aacute;c, kinh doanh v&agrave; chuy&#7875;n giao b&#7843;n quy&#7873;n&hellip; m&agrave; kh&ocirc;ng c&oacute; s&#7921; &#273;&#7891;ng &yacute; tr&#432;&#7899;c b&#7857;ng v&#259;n b&#7843;n c&#7911;a B&ecirc;n A &#273;&#7873;u l&agrave; h&agrave;nh vi vi ph&#7841;m ph&aacute;p lu&#7853;t v&#7873; S&#7903; h&#7919;u tr&iacute; tu&#7879;. Tr&#432;&#7901;ng h&#7907;p B&ecirc;n A ho&#7863;c B&ecirc;n th&#7913; ba &#273;&#432;&#7907;c B&ecirc;n A chuy&#7875;n nh&#432;&#7907;ng t&agrave;i kho&#7843;n c&oacute; h&agrave;nh vi vi ph&#7841;m th&igrave; ph&#7843;i ch&#7883;u tr&aacute;ch nhi&#7879;m theo quy &#273;&#7883;nh c&#7911;a Ph&aacute;p lu&#7853;t Vi&#7879;t Nam v&agrave; b&#7891;i th&#432;&#7901;ng cho B&ecirc;n B to&agrave;n b&#7897; thi&#7879;t h&#7841;i ph&aacute;t sinh do h&agrave;nh vi c&#7911;a m&igrave;nh g&acirc;y ra.</span>
</p>
<p class="c3"><span class="c11">&#272;i&#7873;u 9. VI PH&#7840;M H&#7906;P &#272;&#7890;NG</span></p>
<p class="c3"><span class="c0">N&#7871;u m&#7897;t B&ecirc;n vi ph&#7841;m ngh&#297;a v&#7909; v&agrave; tr&aacute;ch nhi&#7879;m &#273;&atilde; cam k&#7871;t trong h&#7907;p &#273;&#7891;ng n&agrave;y th&igrave; ph&#7843;i ch&#7883;u ph&#7841;t vi ph&#7841;m H&#7907;p &#273;&#7891;ng t&#432;&#417;ng &#273;&#432;&#417;ng 8% ph&#7847;n Gi&aacute; tr&#7883; H&#7907;p &#273;&#7891;ng b&#7883; vi ph&#7841;m. &#272;&#7891;ng th&#7901;i B&ecirc;n vi ph&#7841;m ph&#7843;i b&#7891;i th&#432;&#7901;ng cho B&ecirc;n B&#7883; vi ph&#7841;m to&agrave;n b&#7897; c&aacute;c thi&#7879;t h&#7841;i ph&aacute;t sinh do h&agrave;nh vi vi ph&#7841;m. M&#7913;c b&#7891;i th&#432;&#7901;ng c&#7909; th&#7875; s&#7869; &#273;&#432;&#7907;c C&aacute;c B&ecirc;n th&#7887;a thu&#7853;n v&agrave; c&#259;n c&#7913; tr&ecirc;n thi&#7879;t h&#7841;i th&#7921;c t&#7871; ph&aacute;t sinh do B&ecirc;n B&#7883; vi ph&#7841;m cung c&#7845;p./.</span>
</p>
<p class="c3 c8"><span class="c1 c11"></span></p>
<p class="c3"><span class="c11">&#272;I&#7872;U 10. LU&#7852;T &#272;I&#7872;U CH&#7880;NH V&Agrave; GI&#7842;I QUY&#7870;T TRANH CH&#7844;P</span>
</p>
<p class="c3"><span class="c0">H&#7907;p &#273;&#7891;ng &#273;&#432;&#7907;c gi&#7843;i th&iacute;ch v&agrave; &aacute;p d&#7909;ng theo ph&aacute;p lu&#7853;t hi&#7879;n h&agrave;nh c&#7911;a Vi&#7879;t Nam. Trong tr&#432;&#7901;ng h&#7907;p ph&aacute;t sinh tranh ch&#7845;p, Hai B&ecirc;n s&#7869; &#273;&#432;&#7907;c th&#432;&#417;ng l&#432;&#7907;ng, h&ograve;a gi&#7843;i tr&ecirc;n c&#417; s&#7903; &#273;&ocirc;i b&ecirc;n c&ugrave;ng c&oacute; l&#7907;i. Trong tr&#432;&#7901;ng h&#7907;p c&oacute; nh&#7919;ng tranh ch&#7845;p Hai B&ecirc;n kh&ocirc;ng th&#7875; t&#7921; gi&#7843;i quy&#7871;t v&agrave; ho&agrave; gi&#7843;i, Hai B&ecirc;n s&#7869; &#273;&#432;a ra To&agrave; Kinh T&#7871; n&#417;i B&ecirc;n A &#273;&#7863;t tr&#7909; s&#7903; ch&iacute;nh &#273;&#7875; gi&#7843;i quy&#7871;t. Ph&aacute;n quy&#7871;t c&#7911;a To&agrave; &aacute;n l&agrave; quy&#7871;t &#273;&#7883;nh cu&#7889;i c&ugrave;ng v&agrave; c&oacute; hi&#7879;u l&#7921;c r&agrave;ng bu&#7897;c th&#7921;c hi&#7879;n &#273;&#7889;i v&#7899;i C&aacute;c B&ecirc;n. M&#7885;i chi ph&iacute; ph&aacute;t sinh bao g&#7891;m c&#7843; ph&iacute; lu&#7853;t s&#432; c&#7911;a b&ecirc;n th&#7855;ng ki&#7879;n do b&ecirc;n thua ki&#7879;n ch&#7883;u.</span>
</p>
<p class="c3"><span class="c11">&#272;I&#7872;U 11: T&Iacute;NH TO&Agrave;N V&#7864;N</span></p>
<p class="c3"><span class="c0">C&aacute;c &#273;i&#7873;u kho&#7843;n c&#7911;a H&#7907;p &#273;&#7891;ng n&agrave;y c&ugrave;ng v&#7899;i m&#7885;i Th&#7887;a thu&#7853;n, Ph&#7909; l&#7909;c H&#7907;p &#273;&#7891;ng &#273;&iacute;nh k&egrave;m H&#7907;p &#273;&#7891;ng n&agrave;y ho&#7863;c &#273;&#432;&#7907;c l&#7853;p trong t&#432;&#417;ng lai t&#7841;o th&agrave;nh to&agrave;n b&#7897; Th&#7887;a Thu&#7853;n gi&#7919;a Hai B&ecirc;n. M&#7885;i tr&#432;&#7901;ng h&#7907;p s&#7917;a &#273;&#7893;i, b&#7893; sung H&#7907;p &#273;&#7891;ng n&agrave;y trong qu&aacute; tr&igrave;nh th&#7921;c hi&#7879;n &#273;&#7873;u ph&#7843;i &#273;&#432;&#7907;c l&#7853;p th&agrave;nh Ph&#7909; l&#7909;c H&#7907;p &#273;&#7891;ng. C&aacute;c ph&#7909; l&#7909;c, v&#259;n b&#7843;n &#273;&iacute;nh k&egrave;m H&#7907;p &#273;&#7891;ng l&agrave; b&#7897; ph&#7853;n kh&ocirc;ng th&#7875; t&aacute;ch r&#7901;i c&#7911;a H&#7907;p &#273;&#7891;ng n&agrave;y.</span>
</p>
<p class="c3"><span class="c11">&#272;I&#7872;U 12: CAM K&#7870;T CHUNG</span></p>
<p class="c3"><span class="c0">- Hai B&ecirc;n cam k&#7871;t th&#7921;c hi&#7879;n &#273;&#7847;y &#273;&#7911; c&aacute;c &#273;i&#7873;u kho&#7843;n &#273;&atilde; ghi trong H&#7907;p &#273;&#7891;ng. </span>
</p>
<p class="c3"><span class="c0">- Hai B&ecirc;n &#273;&atilde; &#273;&#7885;c, hi&#7875;u r&otilde; v&agrave; nh&#7845;t tr&iacute; v&#7899;i n&#7897;i dung c&#7911;a H&#7907;p &#273;&#7891;ng.</span>
</p>
<p class="c3"><span class="c0">- H&#7907;p &#273;&#7891;ng n&agrave;y c&oacute; hi&#7879;u l&#7921;c t&#7915; ng&agrave;y k&yacute; v&agrave; l&#7853;p th&agrave;nh 03 (ba) b&#7843;n c&oacute; gi&aacute; tr&#7883; ph&aacute;p l&yacute; nh&#432; nhau. B&ecirc;n A gi&#7919; 01 (m&#7897;t) b&#7843;n, B&ecirc;n B gi&#7919; 02 (hai) b&#7843;n l&agrave;m c&#259;n c&#7913; th&#7921;c hi&#7879;n.</span>
</p>
<p class="c3 c8"><span class="c1 c9"></span></p><a id="t.0170af41292e6b13dc26a180dc744933a81ff316"></a><a id="t.0"></a>
<table class="c23">
    <tbody>
    <tr class="c14">
        <td class="c20" colspan="1" rowspan="1"><p class="c17"><span class="c11">B&Ecirc;N A</span></p>
            <p class="c17"><span class="c0">(K&iacute; v&agrave; ghi r&otilde; h&#7885; t&ecirc;n)</span></p></td>
        <td class="c20" colspan="1" rowspan="1"><p class="c17"><span
                class="c11">&#272;&#7840;I DI&#7878;N B&Ecirc;N B</span></p>
            <p class="c17"><span class="c0">(K&iacute; v&agrave; &#273;&oacute;ng d&#7845;u)</span></p>
            <p class="c5"><span class="c1 c9"></span></p>
            <p class="c2"><span class="c1 c9"></span></p>
            <p class="c2"><span class="c1 c9"></span></p>
            <p class="c17"><span class="c11">NGUY&#7876;N DANH T&Uacute;</span></p></td>
    </tr>
    </tbody>
</table>
<p class="c3 c8"><span class="c1 c9"></span></p>
<div><p class="c12"><span class="c22">H&#272;&#272;TNN-TPE_CN_ver141216&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
</p></div>
</body>
</html>