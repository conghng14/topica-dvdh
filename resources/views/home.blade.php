@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-7 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tin tức</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<ul class="list-unstyled timeline">
					<li>
					  <img src="{{ asset('imgs/mrBean.jpg') }}" alt="..." style="float:left;width:76px;margin-right:10px" />
					  <div class="block_content">
						<h2 class="title">
						  <a>Chương trình tuyển dụng Chuyên gia Nhân sự - Tuyển dụng Quốc tế Tương lai</a>
						</h2>
						<div class="byline">
						  <span>13 hours ago</span> by <a>Jane Smith</a>
						</div>
						<p class="excerpt">
							Chương trình tuyển dụng CHUYÊN GIA NHÂN SỰ - TUYỂN DỤNG QUỐC TẾ TƯƠNG LAI (FISE) là chương trình tìm kiếm những ứng viên có tố chất và đam mê lĩnh vực nhân sự - tuyển dụng với định hướng phát triển theo hướng chuyên gia.
							<a>Read&nbsp;More</a>
						</p>
					  </div>
					</li>
					<li>
					  <img src="{{ asset('imgs/mrBean.jpg') }}" alt="..." style="float:left;width:76px;margin-right:10px" />
					  <div class="block_content">
						<h2 class="title">
							<a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
						</h2>
						<div class="byline">
						  <span>13 hours ago</span> by <a>Jane Smith</a>
						</div>
						<p class="excerpt">
							WORKSHOP GIÚP TÔI HIỂU ĐƯỢC TÍNH CHẤT MÔN HỌC NÀY
							<a>Read&nbsp;More</a>
						</p>
					  </div>
					</li>
					<li>
					  <img src="{{ asset('imgs/mrBean.jpg') }}" alt="..." style="float:left;width:76px;margin-right:10px" />
					  <div class="block_content">
						<h2 class="title">
							<a>Ra mắt Native GEN4 - Chương trình tiếng Anh thế hệ thứ 4 của Topica Native</a>
						</h2>
						<div class="byline">
						  <span>13 hours ago</span> by <a>Jane Smith</a>
						</div>
						<p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
						</p>
					  </div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-5 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tin nổi bật</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<ul class="list-unstyled timeline">
					<li>
					  <img src="{{ asset('imgs/mrBean.jpg') }}" alt="..." style="float:left;width:76px;margin-right:10px" />
					  <div class="block_content">
						<h2 class="title">
						  <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
						</h2>
						<div class="byline">
						  <span>13 hours ago</span> by <a>Jane Smith</a>
						</div>
						<p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
						</p>
					  </div>
					</li>
					<li>
					  <img src="{{ asset('imgs/mrBean.jpg') }}" alt="..." style="float:left;width:76px;margin-right:10px" />
					  <div class="block_content">
						<h2 class="title">
							<a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
						</h2>
						<div class="byline">
						  <span>13 hours ago</span> by <a>Jane Smith</a>
						</div>
						<p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
						</p>
					  </div>
					</li>
				</ul>
			</div>
		</div>
	</div>              
</div>
@endsection
@section('js')
    @parent

    <script src="{{ asset('libs/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('libs/build/js/custom.min.js') }}"></script>
@endsection