@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12" style="min-height:500px">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Danh sách Tài liệu</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div id="ajax" class="demo"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('libs/jstree/style.css') }}" rel="stylesheet">
@endsection

@section('js')
    @parent
    <script src="{{ asset('libs/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('libs/build/js/custom.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('libs/jstree/jstree.min.js') }}" type="text/javascript"></script>
    
    <script>
        $(function(){
            
        });
        $('#ajax').on("changed.jstree", function (e, data) {
            if(data.instance.get_node(data.selected[0]).a_attr.href.length > 1) {
                window.open(data.instance.get_node(data.selected[0]).a_attr.href, '_blank');
            }
        })
        .jstree({
            'core' : {
                'data' : {
                    "url" : "/documents/get-documents",
                    "dataType" : "json" // needed only if you do not supply JSON headers
                }
            }
        });
    </script>
@endsection
