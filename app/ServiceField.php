<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceField extends Model
{
    protected $fillable = [
        'service_id', 'field_id'
    ];

    public function field(){
    	return $this->belongsTo(Field::class);
    }
}
