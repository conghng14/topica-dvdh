<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataCommon extends Model
{
    protected $fillable = [
        'title', 'parent_id'
    ];
}
