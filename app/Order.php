<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
    protected $fillable = [
        'title', 'contact_id','status', 'content', 
        'student_name', 'received_by_name', 'created_by_name', 'service_name', 'service_id',
        'reason', 'service_id', 'feedback', 'created_by_id', 'received_by_id', 'file_path',
        'shipping_status', 'shipping_status_details'
    ];

    protected $casts = [
        'feedback' => 'array'
    ];

    protected $appends = ['progress_bar', 'status_name', 'warning'];

    public function created_by(){
    	return $this->belongsTo(User::class, 'created_by_id', 'id');
    }

    public function received_by(){
        return $this->belongsTo(User::class, 'received_by_id', 'id');
    }

    public function orderService(){
        return $this->hasMany(OrderService::class);
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'created_by_id');
    }

    public function processer(){
        return $this->belongsTo(User::class, 'received_by_id', 'id');
    }

    public function getProgressBarAttribute(){
        $progressBar = 0;
        if($this->status == "DONE"){
            $progressBar = 100;
        }else{
            $steps = OrderStep::where('order_id', $this->id)->get();
            $step_count = $steps->count();
            $step_count_done = $steps->where('status', 1)->count();
            if($step_count > 0){    
                $progressBar = ($step_count_done * 100) / $step_count;
            }else{
                $progressBar = 0;
            }
        }
        
        return number_format($progressBar,0);
    }

    public function steps(){
        return $this->hasMany(OrderStep::class);
    }

    public function getStatusNameAttribute(){
        switch ($this->status) {
            case "NEW":
                return "Mới tạo";
                break;
            case "PENDING":
                return "Đang chờ";
                break;
            case "RECEIVED":
                return "Đang xử lý";
                break;
            case "REJECTED":
                return "Từ chối";
                break;
            case "DONE":
                return "Hoàn thành";
                break;
            default:
                return $this->status;
        }
    }

    public function getWarningAttribute(){
        $today = Carbon::now();
        $diffDay = $today->diffInDays($this->created_at);
        $warning = '<span class="label label-warning">Warning</span>';
        $danger = '<span class="label label-danger">Danger</span>';
        $hop_dong = getServiceIds('hop-dong');
        $hoa_don_do = getServiceIds('hoa-don-do');
        $cod = getServiceIds('cod');
        $rut_hoc_phi = getServiceIds('rut-hoc-phi');
        $hanh_chinh = getServiceIds('hanh-chinh');
        $khuou_nai = getServiceIds('khuou-nai');
        if(in_array($this->status, ['PENDING', 'RECEIVED'])){
            if(in_array($this->service_id, $hop_dong)){
                if($diffDay ==4){
                    return $warning;    
                }elseif($diffDay > 4){
                    return $danger;
                }else{
                    return '';
                }
            }elseif(in_array($this->service_id, $hoa_don_do)){
                if($diffDay ==15){
                    return $warning;    
                }elseif($diffDay > 15){
                    return $danger;
                }else{
                    return '';
                }
            }elseif(in_array($this->service_id, $cod) || in_array($this->service_id, $hanh_chinh) || in_array($this->service_id, $khuou_nai)){
                if($diffDay ==1){
                    return $warning;    
                }elseif($diffDay>1){
                    return $danger;
                }else{
                    return '';
                }
            }elseif(in_array($this->service_id, $rut_hoc_phi)){
                if($diffDay ==12){
                    return $warning;    
                }elseif($diffDay>12){
                    return $danger;
                }else{
                    return '';
                }
            }else{
                return '';
            }
        }else{
            return '';
        }
        
    }
}
