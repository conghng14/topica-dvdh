<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderServiceData extends Model
{
	protected $table = 'order_service_data';
    protected $fillable = [
        'order_service_id', 'service_field_id', 'keyword', 'data_common_id', 'data'
    ];

    protected $appends = ['field_name'];


    public function getFieldNameAttribute(){
    	$field = Field::where('keyword', 'LIKE', $this->keyword)->first();
    	if($field){
    		return $field->title;
    	}else{
    		return "";
    	}
    }
}
