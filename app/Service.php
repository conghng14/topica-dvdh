<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'title', 'parent_id', 'file_path', 'type'
    ];

    public function service_fields(){
    	return $this->hasMany(ServiceField::class)->orderBy('sort');
    }

    public function orderService(){
    	return $this->hasMany(OrderService::class);
    }
}
