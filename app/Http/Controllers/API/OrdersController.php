<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Order;
use App\OrderService;
use App\Field;

class OrdersController extends Controller
{
    public function list($service_id){
        $service_ids = "0," . $service_id;
        $service_ids = explode(",", $service_ids);
    	$date = now();

    	$orderService = OrderService::whereHas('order' , function($q){
    		$q->where('orders.status', '!=', 'NEW');
    	})
    	->whereHas('orderServiceData')
    	->where('pushed', 0)
    	->whereIn('service_id', $service_ids)->get();
		
    	$return = [];
    	foreach($orderService as $k=>$os){
    		$os->pushed = 1;
    		$os->save();
    		foreach($os->orderServiceData as $d){
    			$return[$k]['updated_at'] = $d->updated_at->format('d/m/Y H:i:s');
    			$return[$k]['tvts_email'] = $os->order->user->email;
    			$return[$k]['tvts_full_name'] = $os->order->user->full_name;
    			$return[$k]['tvts_phone'] = $os->order->user->phone;
    			$return[$k][$d->keyword] = $d->data;
    		}
    	}
    	
    	return response()->json(['data' => $return], 200);
    }

    public function listFields(){
    	return response()->json(Field::get(['title','keyword']), 200);
    }
}
