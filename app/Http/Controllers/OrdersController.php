<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Session;
use File;

use App\User;
use App\Order;
use App\Document;
use App\Service;
use App\Field;
use App\OrderService;
use App\OrderServiceData;
use App\Contact;
use App\OrderStep;


class OrdersController extends Controller
{
    
    // 1:TVTS
    // 2:XỬ LÝ
    // 3: GIÁM SÁT
    // 0:ADMIN    
     
    protected $folder_temps   =   "document_template";
    protected $folder_docs    =   "documents";
    protected $field_common   = ['contact_id' ,'ho_ten_hoc_vien','ngay_sinh','so_cmt','ngay_cap_cmt','noi_cap_cmt','so_dien_thoai_hoc_vien','ten_goi_hoc','ma_goi_hoc','hoc_phi_goc','hoc_phi_uu_dai'];

    // public function editData(){
    //     $order = Order::all();
    //     foreach($order as $order){
    //         $created = User::find($order->created_by_id);
    //         if($created){
    //             $order->created_by_name = $created->email;
    //         }
    //         if(isset($order->orderService['0'])){
    //             $service = Service::find($order->orderService['0']->service_id);
    //             if($service){
    //                 $order->service_id = $service->id;
    //                 $order->service_name = $service->title;
    //             }
    //             $order_service_data = OrderServiceData::where('keyword', 'ho_ten_hoc_vien')->where('order_service_id', $order->orderService['0']->id)->first();
    //             if($order_service_data){
    //                 $order->student_name = $order_service_data->data;
    //             }   
    //         }            
            
    //         $received = User::find($order->received_by_id);
    //         if($received){
    //             $order->received_by_name = $received->email;
    //         }
    //         $order->update();
    //     }
    //     return 'success.';
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data_filter = $request;
        $orders = Order::where('created_by_id', '!=', NULL)
            ->orderBy('created_at', 'DESC');
        $user = auth()->user();
        $service_ids = isset($user->role) ? $user->role->service_ids : [];
        if($user->role_id !== 1){ // neu khong phai la Admin
            $this->authorize('view-order'); // kiem tra quyen        
            if(userCant('handle-order') && userCant('assign-order')){ // neu ko phai la giam sat hoac xu-ly
                $orders = $orders->where('created_by_id', auth()->user()->id);
                $orders = $orders->whereHas('orderService');
            }else{
                $orders = $orders->whereHas('orderService')->whereIn('service_id', $service_ids);
            }
        }else{
            $orders = $orders->whereHas('orderService');
        }

        $keyword = $data_filter->keyword or "";

        $orders = $orders->whereHas('orderService.Service');
        $orders = $orders->whereHas('user');
        if($keyword != ""){
            $orders = $orders->where("student_name", "LIKE" , '%' . $keyword . '%')
                ->orWhere("created_by_name", "LIKE" , '%' . $keyword . '%')
                ->orWhere("service_name", "LIKE" , '%' . $keyword . '%')
                ->orWhere("student_name", "LIKE" , '%' . $keyword . '%');
        }        
        $filter_status = $data_filter->filter_status;
        if($filter_status){
            $orders = $orders->where('status', 'LIKE', "$filter_status");
        }

        
        $orders = $orders->paginate(10);
        
        //$orders = $orders->get();        
        //dd($orders->toArray());
        return view("orders.index", compact('orders', 'data_filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $services = Service::where('parent_id', 0)->get();
        $orderServices = [];
        return view("orders.edit", compact('services', 'orderServices'));
    }

    protected function generateFileWord($data, $template_file){
        
        
        $output_folder = public_path('storage/'. $this->folder_docs . '/'. auth()->user()->id);
        $out_file = date('d-m-Y-H-i-s') . '-' . $template_file;
        
        File::makeDirectory($output_folder, 0775, true, true);

        $template_path = storage_path($this->folder_temps . '/' . $template_file);
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template_path);
        foreach($data as $k=>$val){
            $templateProcessor->setValue($k , $val);    
        }           
        $templateProcessor->saveAs($output_folder . '/' . $out_file);
        return $out_file;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {        
        $data = $request->all();
        
        $order_id = $request->id;
        $order_data['created_by_id'] = auth()->user()->id;
        if($order_id){
            $ord = $order->find($order_id);   
            // delete all data order
            $this->deleteOrder($order_id);
            if($ord){
                $order_data['created_by_id'] = $ord->created_by_id;
            }            
        }
        
        $order_data['title'] = $request->title;
        $order_data['contact_id'] = $request->contact_id;
        $services = $request->services;
        $service_data = $request->service_data;        
        // get order data with main service 
        $main_service = Service::find($services[0]);
        if($main_service){
            $order_data["service_id"] = $main_service->id;    
            $order_data["service_name"] = $main_service->title;    
        }
        
        $order_data["created_by_name"] = auth()->user()->email;
        //$order_data["received_by_name"] = "";
        $order_data["student_name"] = isset($service_data[$services[0]]['ho_ten_hoc_vien']) ? $service_data[$services[0]]['ho_ten_hoc_vien'] : "";
        
        $order = $order->create($order_data);
        $order_id = $order->id;
        // check service
        $file_name = null;
        foreach($service_data as $k=>$sd){
            $step_data = [];
            $this->storeToContact($request->contact_id, $sd);
            $service = Service::where('id', $k)->first();
            if($service->type == 'view_file'){
                $file_name = $this->generateFileWord($sd , $service->file_path);
            }elseif($service->type == 'view_form_with_upload_file'){
                $file_name = $this->uploadFile($data);
            }
            $data_order_services['order_id'] = $order_id;
            $data_order_services['service_id'] = $service->id;
            $data_order_services['file_path'] = $file_name;
            $order_service = OrderService::create($data_order_services);
            $order_service_id = $order_service->id;
            
            $idsHopDong = getServiceIds('hop-dong');
            if(in_array($k, $idsHopDong)){
                $step_data[] = ["title" => "In hợp đồng", "keyword"=>"in-hop-dong", "order_id" => $order_id, "is_cod" => 0, "bill_status" => null]; 
                $step_data[] = ["title" => "Xin chữ ký + đóng dấu", "keyword"=>"xin-chu-ky", "order_id" => $order_id, "is_cod" => 0, "bill_status" => null];
            }            
            foreach($sd as $key=>$d){
                if($key == "tang_pham"){
                    $d = implode(",",$d);  
                }
                OrderServiceData::create([
                    'order_service_id' => $order_service_id,
                    'keyword'   => $key,
                    'data'      =>  $d
                ]);
                if($k >=0 && $k <=20){
                    if($key == "nhu_cau_cua_tvts"){
                        $d_step = ["title" => $d, "order_id" => $order_id, "is_cod" => 0, "bill_status" => null, 'keyword' => null];
                        if($d == "Gửi hợp đồng bản cứng cho HV" || $d == "Nhận hợp đồng bản cứng tại văn phòng"){
                            $d_step["keyword"] = 'gui';
                            $d_step["is_cod"] = 1;
                            $d_step["bill_status"] = "Đang gửi";
                        }
                        $step_data[] = $d_step;
                    }
                }
            }
            $idsCod = getServiceIds('cod');
            if(in_array($k, $idsCod) || $k == 22){
                if($k == 24){
                    $step_data[] = ["title" => "Cập nhật mã Member Shipcard", "keyword"=>"cod-thutien", "order_id" => $order_id, "is_cod" => 0, "bill_status" => null];       
                }
                $step_data[] = ["title" => "Gửi cho học viên", "keyword"=>"gui", "order_id" => $order_id, "is_cod" => 1, "bill_status" => "Đang gửi"];   
            }

            if(in_array($k, $idsHopDong) || in_array($k, $idsCod) || $k == 22){
                OrderStep::insert($step_data);
            }
            
            $file_name = null;
        }
         
        
        
        
        return redirect('orders');
    }

    protected function uploadFile($data, $u_id = null){
        if(isset($data['upload_file'])){
            $u_id = $u_id != null ? $u_id : auth()->user()->id;
            $file_name = 'file-upload-' . $data['upload_file']->getClientOriginalName();    
            $data['upload_file']->move(public_path('storage/'. $this->folder_docs . '/'. $u_id), $file_name);
            return $file_name;    
        }else{
            return null;
        }        
    }

    function deleteOrder($order_id){
        if(request()->ajax()) // This is what i am needing.
        {
            if(userCant('delete')){
                return false;
            }
        }
        $order = Order::find($order_id);

        $this->authorize('delete', $order);

        $created_by_id = isset($order->created_by_id) ? $order->created_by_id : 0;
        
        Order::destroy($order_id);
        $orderServices = OrderService::where('order_id', $order_id)->get();
        if($orderServices->count() > 0){
            foreach($orderServices as $orderS){
                $file = $orderS->file_path;
                $file = public_path('storage/'. $this->folder_docs . '/'. $created_by_id . '/' . $file);
                File::delete($file);
                $orderS->delete();
                OrderServiceData::where('order_service_id', $orderS->id)->delete();
            }
        }
        sleep(2);
    }

    public function storeToContact($contact_id, $data){ 
        if($contact_id != null){
            $contact = Contact::where('id', $contact_id)->first();
            if(!$contact){
                $ct_data['contact_id'] = $contact_id;            
                $ct_data['data'] = array_only($data , $this->field_common);

                $ct_data['data']['ho_ten_nguoi_nhan'] = isset($data['ho_ten_hoc_vien']) ? $data['ho_ten_hoc_vien'] : "";
                $ct_data['data']['so_dien_thoai_nguoi_nhan'] = isset($data['so_dien_thoai_hoc_vien']) ? $data['so_dien_thoai_hoc_vien'] : "";
                
                Contact::create($ct_data);
            }
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $this->authorize('view', $order);
        
        //flash()->overlay('Whoop', 'test');        
        return view('orders.show', compact('order'));
    }

    public function changeStatusStep($step_id, $status){
        $step = OrderStep::find($step_id);
        if($step){
            if(auth()->user()->id != $step->order->received_by_id){
                return 2;
            }
            // if($step->is_cod == 1){
            //     if($step->bill_status != "DONE"){
            //         return 3;
            //     }
            // }
            if(in_array($status, [0,1])){             
                $step->status = $status;
                $step->save();
                return 1;   
            }
        }
        return 0;
    }

    public function changeStatus($id, $status){
        //flash()->overlay('Whoop', 'test');
        $user_id = auth()->user()->id;
        $order = Order::where('id', $id)
            //->where('created_by_ids', $user_id)
            //->orWhere('received_by_id', $user_id)
            ->first();
        $current_status = $order->status;
        $order->status = $status;
        if($current_status != "NEW" && $current_status != "PENDING"){
            if($user_id != $order->received_by_id){
                return "NO_PERMISSION";
            }
            if($status == "DONE"){
                if($order->steps->count() > 0){
                    $done = 1;
                    foreach($order->steps as $step){
                        if($step->status == 0){
                            $done = 0;
                        }
                    }
                    if($done == 0){
                        return "NOT_DONE";
                    }
                }
            }
        }elseif($status == 'RECEIVED'){
            $order->received_by_id = $user_id;
            $order->received_by_name = auth()->user()->email;

        }else{
            if($user_id != $order->created_by_id){
                return "NO_PERMISSION";
            }
        }        
        $order->update();
        return $status;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::where('id', $id)->with('orderService.orderServiceData', 'orderService.service')->first();
        $this->authorize('update', $order);
        //$services = Service::all();
        $services = Service::where('parent_id', 0)->get();
        $orderServices = [];
        foreach($order->orderService as $order_service){
            $orderServices[] = $order_service->service_id;
        }
        return view('orders.edit', compact('order' , 'services', 'orderServices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        $this->deleteOrder($id);
    }

    public function contentServices(Request $request){
        $val = $request->val;

        $services = config('global.list_services');
        return $services[$val]['content'];
        //$x = array_pluck($settings, 'siteTitle');
        //return view("orders.edit", ['test' => $settings['hoa-don-do-moi']]);

    }

    public function generateServiceDetails($id, $contact_id, $order_id){
        $service = Service::where('id', $id)->with('service_fields.field')->first();
        //dd($service->toArray());
        $contact = Contact::where('contact_id', $contact_id)->first();
        $contact_data = $contact ? $contact->data : [];

        $order_service_data = [];
        if($order_id > 0){
            $orderService = OrderService::where('order_id', $order_id)->where('service_id', $id)->with('OrderServiceData')->first();
        
            foreach($orderService->orderServiceData as $d){
                $order_service_data[$d->keyword] = trim($d->data);
            }
        }
        if(isset($order_service_data['tang_pham'])){
            $order_service_data['tang_pham'] = explode(",", $order_service_data['tang_pham']);
        }

        return view('orders.generateServiceDetails', compact('service', 'contact_data', 'order_service_data', 'contact_id'));

    }

    public function getServices($parent_id = 999999, $order_id = 0){
        $p_ids[] = 999999;
        $p_ids[] = $parent_id;
        $service_ids = auth()->user()->role->service_ids;        
        $services = Service::whereIn('parent_id', $p_ids);
        if(auth()->user()->role_id != 1){
            $services = $services->whereIn('id', $service_ids);
        }        
        $services = $services->get();

        $order = Order::where('id', $order_id)->with('orderService.orderServiceData', 'orderService.service')->first();
        $orderServices = [];
        if(isset($order->orderService)){
            foreach($order->orderService as $order_service){
                $orderServices[] = $order_service->service_id;
            }            
        }
        return view('orders.getServices', compact('services', 'orderServices'));
    }

    public function downloadFile($order_id, $file_name){
        $order = Order::where('id', $order_id)->first();

        if($order){
            $file = public_path('storage/'.$this->folder_docs . '/'. $order->created_by_id . '/' . $file_name);   
            
            $headers = array(
              'Content-Type: application/msword',
            );

            return response()->download($file, $file_name, $headers); 
        }
    }

    public function preview(Request $request){
        $service_data = $request->service_data;
        // check service
        $file_name = null;
        foreach($service_data as $k=>$sd){
            $service = Service::where('id', $k)->first();
            if($service->type == 'view_file'){
                $this->folder_docs = "preview/" . $service->id;
                $file_name = $this->generateFileWord($sd , $service->file_path);

                $output_folder = public_path('storage/preview/'. $service->id . '/'. auth()->user()->id);
                
                File::makeDirectory($output_folder, 0775, true, true);
                $file_preview = url('/storage/preview/'. $service->id . '/'. auth()->user()->id . '/' . $file_name);
                sleep(2);
                return redirect('https://docs.google.com/viewerng/viewer?url=' . $file_preview);
            }
        }
    }

    public function stepProcess(Request $request, $order_id){        
        $order = Order::find($order_id);
        if($order){
            if($request->type == "ud_so_hd"){
                $order->so_hop_dong = $request->data;
                $order->update();
                OrderStep::where('order_id', $order_id)->where('keyword', 'LIKE' , 'xin-chu-ky')->update(['status' => 1]);    
            }elseif($request->type == "ud_dvvc"){                
                $order->shipping_unit = $request->data;
                $order->shipping_date_success = caculateShippingDate($request->data);
                if($request->ma_vandon != null){
                    $order->ma_vandon = $request->ma_vandon;
                }else{
                    $order->ma_vandon = getMaVanDon($request->data);
                }
                if($request->shipping_status != null){
                    $order->shipping_status = $request->shipping_status;
                }else{
                    $order->shipping_status = "Đang chuyển";
                }                
                $order->update();
                if($request->shipping_status == 'Hoàn thành'){
                    OrderStep::where('order_id', $order_id)->where('keyword', 'LIKE' , 'gui')->update(['status' => 1]);    
                }else{
                    OrderStep::where('order_id', $order_id)->where('keyword', 'LIKE' , 'gui')->update(['status' => 0]);
                }
            }elseif($request->type == "member_shipcard"){            
                $order->member_shipcard = $request->data;
                $order->update();
                OrderStep::where('order_id', $order_id)->where('keyword', 'LIKE' , 'cod-thutien')->update(['status' => 1]);    
            }
        }
    }

    public function uploadScan(Request $request, $order_id){
        $order = Order::find($order_id);
        $data = $request->all();        
        $file_name = $this->uploadFile($data, $order->created_by_id);
        if($file_name != null){
            $order->file_scan = $file_name;
            $order->update();
            OrderStep::where('order_id', $order_id)->where('keyword', 'LIKE' , 'scan-hop-dong')->update(['status' => 1]);    
        }        
        return redirect('orders/show/'. $order_id);
    }
}
