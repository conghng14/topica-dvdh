<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Document;
use App\DocumentCategory;
use File;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documentCategory = DocumentCategory::with(['childs', 'documents'])->whereNull('parent_id')->get();
        //dd($documentCategory->toArray());
        return view("documents.index", compact('documentCategory'));
    }

    public function getDocuments(){
        $documentCategory = DocumentCategory::with(['childs', 'documents'])->whereNull('parent_id')->get();
        $data = [];
        
        foreach($documentCategory as $k=>$documentCat){
            $data[$k]["id"]   =   $documentCat->id;
            $data[$k]["text"]   =   $documentCat->title;
            $data[$k]["state"]   =   ["opened" => true];
            $i = 0;
            foreach($documentCat->childs as $child){
                $data[$k]['children'][$i]['text'] = $child->title;
                $j=0;
                foreach($child->childs as $c){
                    $data[$k]['children'][$i]['children'][$j]['text'] = $c->title;
                    $j++;
                }
                foreach($child->documents as $d){
                    $data[$k]['children'][$i]['children'][$j]['text'] = $d->title;
                    $data[$k]['children'][$i]['children'][$j]['icon'] = "jstree-file";
                    if($d->type == "download_file"){
                        $data[$k]['children'][$i]['children'][$j]['a_attr']['href'] = "documents/download-documents/" . $d->id;    
                    }else{
                        $data[$k]['children'][$i]['children'][$j]['a_attr']['href'] = $d->file_path;
                    }
                    
                    // $data[$k]['children'][$i]['children'][$j]['state'] = ["selected" => true];
                    $j++;
                }

                $i++;
            }
            foreach($documentCat->documents as $c => $document){
                $data[$k]['children'][$i]['text'] = $document->title;
                $data[$k]['children'][$i]['icon'] = "jstree-file";
                if($document->type == "download_file"){
                    $data[$k]['children'][$i]['a_attr']['href'] = "documents/download-documents/" . $document->id;
                }else{
                    $data[$k]['children'][$i]['a_attr']['href'] = $document->file_path;
                }
                
                $i++;
            }

        }

        // $data[0]['id'] = "a-1";
        // $data[0]['text'] = 'conghc';

        // $data[1]['id'] = "a-2";
        // $data[1]['text'] = 'quanhc';
        //$x = '[{"id":1,"text":"Root node","children":[{"id":2,"text":"Child node 1"},{"id":3,"text":"Child node 2"}]}]';
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('111');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadFile($id){
        $document = Document::find($id);

        if($document){
            $file_name = $document->file_path ? $document->file_path : "doc.docx";
            $file = storage_path('office_template/'. $file_name);   
            
            $headers = array(
              'Content-Type: application/msword',
            );
            if(File::exists($file)){
                return response()->download($file, $file_name, $headers); 
            } else {  
                dd("File not exists");
            }            
        }
    }
}
