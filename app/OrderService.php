<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderService extends Model
{
    protected $fillable = [
        'order_id', 'service_id', 'pushed', 'file_path'
    ];

    public function orderServiceData(){
        return $this->hasMany(OrderServiceData::class)->orderBy('id');
    }

    public function service(){
    	return $this->belongsTo(Service::class);
    }

    public function order(){
    	return $this->belongsTo(Order::class);
    }    
}
