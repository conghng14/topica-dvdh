<?php

namespace App\Policies;

use App\User;
use App\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    public function before(User $user){
        if($user->role_id === 1){
            return true;
        }
    }

    /**
     * Determine whether the user can view the order.
     *
     * @param  \App\User  $user
     * @param  \App\Order  $order
     * @return mixed
     */
    public function view(User $user, Order $order)
    {
        if(!isset($order->orderService[0])){
            return false;
        }else{
            if(!$user->role) return false;
            if(!$user->role->service_ids) return false;
            $orderServiceId = $order->orderService[0]->service_id;
            // return in_array($orderServiceId, $user->role->service_ids)
            //         && $user->can('view-order') && $user->id === $order->created_by_id;
            if($user->id === $order->created_by_id){
                return true;
            }else{
                return in_array($orderServiceId, $user->role->service_ids) && $user->can('view-order');
            }
        }
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param  \App\User  $user
     * @param  \App\Order  $order
     * @return mixed
     */
    public function update(User $user, Order $order)
    {
        if($user->id === $order->created_by_id || $user->role_id === 1){
            return true;
        }
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param  \App\User  $user
     * @param  \App\Order  $order
     * @return mixed
     */
    public function delete(User $user, Order $order)
    {
        if($user->id === $order->created_by_id || $user->role_id === 1){
            return true;
        }
    }
}
