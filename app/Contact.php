<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'contact_id', 'data'
    ];

    protected $casts = [
        'data'   => 'array',
    ];
}
