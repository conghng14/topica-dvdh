<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
	protected $table = "document_category";
    
	public function childs(){
		return $this->hasMany(DocumentCategory::class, 'parent_id', 'id')->with('childs', 'documents');
	}

	public function parent()
    {
        return $this->belongsTo(DocumentCategory::class, 'parent_id');
    }

    public function documents(){
    	return $this->hasMany(Document::class);
    }
}
