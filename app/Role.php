<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $casts = [
		'services' => 'array'
	];
	protected $appends = [
		'service_ids'
	];

    public function permissions(){
    	return $this->belongsToMany(Permission::class, 'role_permission');
    }

    public function getServiceIdsAttribute(){
    	if($this->parent_id){
    		$parent = Role::find($this->parent_id);
    		return $parent->services;
    	}else{
    		return null;
    	}
    }
}
