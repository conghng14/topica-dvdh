<?php

namespace App\Tasks;

use App\Order;
use App\OrderStep;

class UpdateShippingStatus
{
    /**
     * Run the task.
     *
     * @return void
     */
    public function run()
    {
        $this->clearOldCommands();
    }

    /**
     * Clear old commands.
     *
     * @return void
     */
    protected function clearOldCommands()
    {        
        $shipping_unit_arr = ['Viettel miền bắc', 'Viettel miền nam', 'Giao hàng tiết kiệm']; 
            //'Viet-air', 
            
        $str_done = 'Hoàn thành';

        $orders = Order::whereIn('shipping_unit', $shipping_unit_arr)
            ->where('shipping_status', '!=', $str_done)->where('ma_vandon', '!=', null)->get();
        foreach($orders as $order){
            if(in_array($order->shipping_unit, ['Viettel miền bắc', 'Viettel miền nam'])){
                $data = $this->viettel($order->ma_vandon);
                if($data['shipping_status'] != ''){
                    if($data['shipping_status'] != $order->shipping_status){
                        $order->update($data);
                        if($data['shipping_status'] == $str_done){
                            OrderStep::where('order_id', $order->id)->where('keyword', 'gui')->update(['status' => 1]);
                        }
                    }   
                }              
            }elseif($order->shipping_unit == 'Viet-air'){
                //$data = $this->vietAir($order->ma_vandon);
                //$order->update($data);
            }else{
                $data = $this->ghtk($order->ma_vandon);
                if($data['shipping_status'] != ''){
                    if($data['shipping_status'] != $order->shipping_status){
                        $order->update($data);
                        if($data['shipping_status'] == $str_done){
                            OrderStep::where('order_id', $order->id)->where('keyword', 'gui')->update(['status' => 1]);
                        }
                    }   
                }
            }
        }
        
    }

    protected function viettel($ma_vandon){
        $ma_vandon = 'PMI1401031886';
        $url = 'https://www.viettelpost.com.vn/Tracking?KEY=' . $ma_vandon;
        $html = new \Htmldom($url);

         $status = $details = '';

        foreach($html->find('div[class=divhead1] span[class=spantop] span') as $element) 
            $status = $element->innertext;
        if(isset($status)){
            $status = $status == 'Phát thành công' ? 'Hoàn thành' : $status;
            
            foreach($html->find('div[class=trackingItem] ul') as $element) 
            $details = $element->outertext;
        }
        
        return ['shipping_status' => $status, 'shipping_status_details' => $details];
    }

    protected function vietAir(){
        // $ma_vandon = 'va0162204';
        // $url = 'http://viet-air.com/kiem-tra-don-hang?code=' . $ma_vandon;
        // $html = new \Htmldom($url);

        // foreach($html->find('div[class=divhead1] span[class=spantop] span') as $element) 
        //     $status = $element->innertext;
        // $status = $status == 'Phát thành công' ? 'Hoàn thành' : $status;
        
        // foreach($html->find('div[class=trackingItem] ul') as $element) 
        //     $status_details = $element->outertext;
        // return ['shipping_status' => $status, 'shipping_status_details' => $status_details];
    }

    protected function ghtk(){
        $ma_vandon = 'S9453.MN2.A14.76434301';
        $url = 'http://services.giaohangtietkiem.vn/services/shipment/v2/' . $ma_vandon;

        $client = new \GuzzleHttp\Client();
        $status = $details = '';
        $res = $client->get($url,
                ['headers' => [
                    'Token' => '8C9212EDc60705EFB21Db51b24FAE14Ae22101cc',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                    ]
                ]
            );
        $rt = json_decode($res->getBody()->getContents());
        if(isset($rt->order->status_text)){
            $status = $rt->order->status_text;
            $status = $status == 'Đã đối soát' ? 'Hoàn thành' : $status;

            $details = '<b>Họ tên người nhận:</b> '. $rt->order->customer_fullname . '<br />';
            $details .= '<b>Số đt người nhận:</b> '. $rt->order->customer_tel . '<br />';
            $details .= '<b>Địa chỉ:</b> '. $rt->order->address;
        }
        

        return ['shipping_status' => $status, 'shipping_status_details' => $details];
    }
}
