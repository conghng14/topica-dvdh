<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStep extends Model
{
    
	protected $fillable = [
        'title', 'keyword','order_id', 'status', 
        'is_cod', 'bill_status'
    ];
    
    function order(){
    	return $this->belongsTo(Order::class);
    }
}
