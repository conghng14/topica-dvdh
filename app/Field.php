<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'title', 'keyword', 'type', 'data_common_id'
    ];

    protected $appends = [
    	'data_select'
    ];


    public function data_common(){
    	return $this->belongsTo(DataCommon::class)->orderBy('id');
    }

    public function getDataSelectAttribute(){
    	if($this->data_common_id != null){
    		return DataCommon::where('parent_id', $this->data_common_id)->orderBy('id')->get();	
    	}else{
    		return null;
    	}
    }
}
