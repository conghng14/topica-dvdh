<?php
use Carbon\Carbon;
use App\Order;

	function getServiceIds($name = "")
	{
		switch ($name) {
		    case "hop-dong":
		        return [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
		        break;
		    case "hoa-don-do":
		        return [22,23];
		        break;
		    case "cod":
		        return [24,25,26,27];
		        break;
		   	case "rut-hoc-phi":
		        return [28];
		        break;
		    case "hanh-chinh":
		        return [30,31,32,33];
		        break;
		    case "khuou-nai":
		        return [38];
		        break;
		    default:
		        return [0];
		}
	}

	function caculateShippingDate($shipping_unit = ''){
		$today = Carbon::now();
		switch ($shipping_unit) {
		    case "Viet-air":
		    case "viet-air":
		        return $today->addDays(3);
		        break;
		    case "Viettel miền bắc":
		    case "Viettel miền nam":
		    case "viettel":
		        return $today->addDays(5);
		        break;
		    case "Giao hàng tiết kiệm":
		    case "ghtk":
		        return $today->addDays(3);
		        break;
		   	case "TVTS":
		   	case "tvts":
		    case "nv":
		    case "shipper":
		    case "ong-vang":
		        return $today->addDays(1);
		        break;
		    default:
		        return $today->addDays(1);
		}
	}

	function getMaVanDon($shipping_unit = ''){
		$prefix = '';
		$max_ma_vandon = Order::where('ma_vandon', '!=', null)->whereDate('updated_at', '=', date('Y-m-d'))->max('ma_vandon');
		
		if($shipping_unit == 'Viettel miền bắc'){
			$prefix = 'PMI14';
		}elseif($shipping_unit == 'Viettel miền nam'){
			$prefix = 'SN';
		}
		$ma_vandon = $prefix;
	    if(!$max_ma_vandon){
	        $ma_vandon .= date("dmy"). "01";
	    }else{
	        $suffix = (int) substr($max_ma_vandon, -2) + 1;
	        $suffix = $suffix < 10 ? "0" . $suffix : $suffix;
	        $ma_vandon .= date("dmy"). $suffix;
	    }
	    return $ma_vandon;
	}