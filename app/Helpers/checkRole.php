<?php
use App\User;
use App\Department;
use App\Order;

//private function 
function userCan($role = "")
{
	if(auth()->user()->role_id === 1) return true;
	return auth()->user()->can($role);
}

function userCant($role = ""){
	if(auth()->user()->role_id === 1) return false;
	return auth()->user()->cant($role);   	
}
